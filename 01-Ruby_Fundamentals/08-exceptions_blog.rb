# encoding: utf-8

require_relative '08-blog'
require_relative '08-tweetable'

post = Blog::Post.new author: "Kiruban PREMKUMAR ééé",
                      title: "rails rocks ééé",
                      #body: "some lorem text.. ééé",
                      body: nil,
                      comments: Blog::Comment.new(user: "Mika", body: "nope, js rocks.")

post.extend Tweetable

begin
  post.tweet
rescue Tweetable::NoBodyError
  puts "No body was in the post, tweet could not be sent."
ensure
  # close a file for example
end
