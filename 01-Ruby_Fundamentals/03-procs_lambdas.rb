def form &block
  puts "<form>"
  yield if block_given?
  puts "</form>"
end

def form_with_proc p
  puts "<form>"
  p.call
  puts "</form>"
end

def paragraph text
  puts "<p>" + text + "</p>"
end

def quote text
  puts "<blockquote>" + text + "</blockquote>"
end

# Execute Something

form do
  paragraph "This is a prograph."
  quote "This is a quote from Kiruban"
end

myproc = proc do
  paragraph "This is a paragraph"
  quote "This is a quote from Kiruban"
end

form_with_proc myproc
