def form &block
  puts "<form>"
  yield if block_given?
  puts "</form>"
end

def paragraph text
  puts "<p>" + text + "</p>"
end

def quote text
  puts "<blockquote>" + text + "</blockquote>"
end

# Execute Something

form do
  paragraph "This is a prograph."
  quote "This is a quote from Kiruban"
end
