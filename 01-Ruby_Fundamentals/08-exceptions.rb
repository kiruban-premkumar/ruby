begin
  raise ArgumentError, "Forcing the exception to be raised."
rescue ArgumentError => e
  puts "Missing arguments maybe?: #{e.class}"
rescue => e
  puts "Oops!"
end
