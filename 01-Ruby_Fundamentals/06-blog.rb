# encoding: utf-8

require_relative '06-modules'

p Blog

post = Blog::Post.new author: "Kiruban PREMKUMAR ééé",
                      title: "rails rocks ééé",
                      body: "some lorem text.. ééé",
                      comments: Blog::Comment.new(user: "Mika", body: "nope, js rocks.")

p post


post2 = Blog::Post.new author: "Kiruban PREMKUMAR ééé",
                      title: "rails rocks ééé",
                      body: "some lorem text.. ééé"

post2.insert_random_comment

p post2

post3 = Blog::Post.new author: "Kiruban PREMKUMAR ééé",
                      title: "rails rocks ééé",
                      body: "some lorem text.. ééé"

post3.tweet

p post3

post4 = Blog::Post.new author: "Kiruban PREMKUMAR ééé",
                      title: "rails rocks ééé",
                      body: "some lorem text.. ééé"

post4.insert_random_comment
post4.insert_random_comment
post4.insert_random_comment

post4.comments.each { |c| c.tweet }
p post4
