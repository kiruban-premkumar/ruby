# encoding: utf-8

require_relative '06-modules-2'

p Blog

post = Blog::Post.new author: "Kiruban PREMKUMAR ééé",
                      title: "rails rocks ééé",
                      body: "some lorem text.. ééé",
                      comments: Blog::Comment.new(user: "Mika", body: "nope, js rocks.")

post2 = Blog::Post.new author: "Kiruban PREMKUMAR ééé",
                      title: "rails rocks ééé",
                      body: "some lorem text.. ééé",
                      comments: Blog::Comment.new(user: "Mika", body: "nope, js rocks.")

post.extend Tweetable

# will work
post.tweet

# will not work
post2.tweet
