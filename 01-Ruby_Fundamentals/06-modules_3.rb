module Greeter
  def hello name
    puts "Hello #{name}!"
  end

  def goodbye name
    puts "Goodbye #{name}."
  end
end

module Human
  class Person
    include Greeter
    def initialize name
      @name = name
    end
  end
end

person = Human::Person.new "Kiruban"
puts person.inspect
