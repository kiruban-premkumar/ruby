# encoding: utf-8

# Classes
class Post
  attr_reader :author, :title, :body, :comments

  def initialize options
    @author = options[:author]
    @title = options[:title]
    @body = options[:body]
    @comments = options[:comments] || []
  end

  def insert_comment *comments
    comments.each { |c| @comments << c  }
  end

end

class Comment
  attr_reader :user, :body

  def initialize options
    @user = options[:user]
    @body = options[:body]
  end
end

# Program

post = Post.new author: "Kiruban PREMKUMAR",
                title: "Why ruby rocks !",
                body: "Rails is awesome"

post.insert_comment Comment.new({ user: "Mickael Mani",
                                  body: "I love JS."
                                }),
                    Comment.new({ user: "Dimitri Segard",
                                  body: "I prefer Python... Ruby Sucks."
                                })
p post.inspect
