# encoding: utf-8

# Classes
class Post
  attr_reader :author, :title, :body, :comments

  def initialize author, title, body, comments = []
    @author = author
    @title = title
    @body = body
    @comments = comments
  end

end

class Comment
  attr_reader :user, :body

  def initialize(user, body)

  end
end

# Program

post = Post.new "Kiruban PREMKUMAR", "Why ruby rocks !", "Rails is awesome", []

p post.inspect

post = Post.new "Kiruban PREMKUMAR", "Why ruby rocks !", "Rails is awesome", []

p post.inspect
