# First way
number = 0
loop do
  break if number > 15
  puts "The number inside the loop is #{number}"
  number += 1
end

# Second Way
number = 0
until number > 15 do
  puts "The number inside the loop is #{number}"
  number += 1
end

# Third Way
number = 0
while number <= 15 do
  puts "The number inside the loop is #{number}"
  number += 1
end

# 4th way

16.times do |number|
  puts "The number inside the loop is #{number}"
end

# 5th way

(0..15).each do |number|
  puts "The number inside the loop is #{number}"
end

(0...16).each do |number|
  puts "The number inside the loop is #{number}"
end

# 6th way

for number in 0..15 do
  puts "The number inside the loop is #{number}"
end
