################

# Affichage
print "Bonjour Kiruban!\nQuel âge as-tu? "

# Entrée utilisateur
age = gets
age.chomp! # Pour enlever le retour chariot
puts "Tu as #{age} ans! :)"

################

puts "Hello World".class
# String
# => nil

puts 3.class
# Fixnum
# => nil

puts 3.14.class
# Float
# => nil

puts true.class
# TrueClass
# => nil

puts false.class
# FalseClass
# => nil

puts [ true, false, 3, "Kiruban PREMKUMAR" ].class
# Array
# => nil

puts nil.class
# NilClass
# => nil

puts :symbol.class
# Symbol
# => nil

puts "symbol"
# symbol
# => nil

puts "symbol".object_id == "symbol".object_id
# false
# => nil

puts :symbol.object_id == :symbol.object_id
# true
# => nil

## Structure conditionnelle : if elsif else

puts "Est-tu un homme ou une femme (H/F) ?"
gender = gets.chomp

if gender == "H"
  puts "T'es un homme!"
elsif gender == "F"
  puts "T'es une femme!"
else
  puts "Je ne connais pas ce genre."
end
