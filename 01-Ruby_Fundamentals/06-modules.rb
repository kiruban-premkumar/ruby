# encoding: utf-8

module Tweetable
  def tweet
    puts "Tweeted the following : #{body}"
  end
end

module Blog
  # Classes
  class Post
    include Tweetable
    attr_reader :author, :title, :body, :comments

    def initialize options
      @author = options[:author]
      @title = options[:title]
      @body = options[:body]
      @comments = options[:comments] || []
    end

    def insert_comment *comments
      comments.each { |c| @comments << c  }
    end

    def insert_random_comment
      @comments << Comment.new(user: "José Mota", body: "random text")
    end

  end

  class Comment
    include Tweetable
    attr_reader :user, :body

    def initialize options
      @user = options[:user]
      @body = options[:body]
    end
  end

end
