require "socket"
webserver = TCPServer.new('localhost',2000)

while(session = webserver.accept)
  response = "Hello World"
  session.print "HTTP/1.1 200 OK\r\n" + 
		"Content-type: text/html\r\n" +
		"Content-Length: #{response.bytesize}\r\n" +
		"Connection: close\r\n"
  session.print "\r\n"
  session.write(response)
  session.close
end
