class Car

  attr_reader :state

  def initialize engine, tires
    @engine = engine
    @tires = tires
  end

  def start
    @state = "running"
  end

  def stop
    @state = "stopped"
  end

end

car = Car.new "My beautifull TDI", [1,2,3,4]

puts car.inspect

car.start
p "The car's engine state is #{car.state}"

car.stop
p "The car's engine state is #{car.state}"
