At last, **Rails**!

## First week

This is the last week on Kitt. We will dive into the architecture of a fresh Rails app
and try to understand what's going on. We'll build one rails app per day.

## Second week

**AirBnB**! For the second week, we'll work as teams of 2 to 4. The goal is to start
from scratch and build a MVP of an AirBnB clone.

- First demo on Wednesday
- Official demo on Friday

Don't be shy! Be proud :)