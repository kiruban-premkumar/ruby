## Welcome!

Let's start your coding challenges on Kitt. This repo holds all the exercises
you'll cover during your 9 weeks at [Le Wagon, the French innovative Coding School](http://www.lewagon.org/en)


## Exercises License

[![Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License](https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-nd/4.0/)