#### `01-Pure-Bootstrap-grid`
A challenge to put all your Bootstrap skills together (utility classes, components, grid) and build a nice landing page. For this challenge, continue to work on the previous project. No need to start from scratch!

#### `02-Advanced-Bootstrap-grid`
Now that you are Bootstrap-grid experts, learn more advanced but very useful techniques with the grid.

#### `03-Bootstrap-mockup-v1`
A challenge to get you familiar with Bootstrap CSS classes:

- utilities classes (`.text-center`, `.list-inline`, etc..)
- UI components classes (`.btn-primary`, `.form-inline`, etc..)

#### `04-Bootstrap-mockup-v2`
A challenge to put all your Bootstrap skills together (utility classes, components, grid) and build a nice landing page. For this challenge, continue to work on the previous project. No need to start from scratch!
