#### `01-Content-migration`

The objective of this challenge is to migrate your poor HTML website into a nice Middleman project with templates and SASS.

#### `02-Style-migration`

In this challenge, you will adopt a very clean organization of our styleheets, and you will use SASS variables, chaining and nesting to have a concise but efficient design.

#### `Optional-01-Local-data`

Add a new page to your site connected to a text-database of your choice. Once you know this technique, you can easily add new content and maintain your website.

#### `Optional-02-Sass-UI-modules`

This challenge makes you write your first SASS modules. Once written, you can re-use them in all your future projects. That rocks!