## Background & Objectives

This challenge makes you write your first SASS modules. Once written, you can re-use them in all your future projects. That rocks!

## Specs

### Button module

Personal button classes are very powerful to improve your site design, since buttons are everywhere. Code your **`components/_button.css.scss`** module with the class of your choice. Here is an example of button module.

```css
.btn-rounded {
  border-radius: 50px;
  padding: 0.3em 1.5em;
}
```

## Tips & Resources

Don't hesitate to create your own generic classes with the design and effect you like. A good tip is to look for inspiring example on [Codepen](http://codepen.io) and then modify these example to create your own classes. You can look at the [top pens '14](http://codepen.io/2014/popular/) to find inspiration.