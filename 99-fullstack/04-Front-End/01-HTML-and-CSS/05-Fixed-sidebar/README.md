## Background & Objectives

Build a nice layout with a fixed sidebar and a scrollable page content.

## Specs

Here is [your objective](http://lewagon.github.io/html-css-challenges/05-fixed-sidebar/). To build such layout, you will have to position the left sidebar as fixed, and add some CSS to get the page content on the right.

## Tips & Resources

Sometimes you need internal links, referring to sections of your page, not to other pages of your site. Here is how you do that:

```html
<!-- In your nav list -->
<a href="#summary">Info</a>
<a href="#favorite-movies">Movies</a>
<a href="#help">Info</a>

<!-- [...] -->

<!-- Now in your page content -->
<div id="summary">your summary</div>
<div id="favorite-movies">your favorite movies</div>
<div id="help">some help section</div>
```

Later on, we will add a nice smoothscroll effect on such links. But wait for next week!

You can read [this post](http://css-tricks.com/absolute-relative-fixed-positioining-how-do-they-differ/) about positioning if you have some free time!

