#### `HTML / CSS`
Discover most standard HTML tags and CSS properties. Master CSS selectors and positioning techniques. Become a web-designer by learning cool CSS tricks.

#### `Bootstrap`
Discover Boostrap CSS/JS library. Grasp all the useful Bootstrap components and CSS classes, and master the responsive grid system.

#### `Front-End advanced`
Discover ERB templating an SASS by playing with a little (but powerful) front-end framework: Middleman. Learn to write elegant, DRY and consistent design with SASS. Prepare yourself for coding killer front-ends in your Rails apps.

#### `Javascript - jQuery`
Learn javascript. Know how to read/write the DOM and define events with jQuery javascript library.

#### `Animations tricks`
Focus on animations. Start coding cool effects using only CSS animations. Then add more advanced animations with jQuery: navbar scroll, scroll reveal, etc..

#### `HTTP & AJAX`
Discover how the web works and what's behind HTTP! Learn how to make HTTP requests in javascript using AJAX and use your new skills to connect to APIs.

#### `Javascript Advanced`
Improve your JS skills on more complicated challenges.