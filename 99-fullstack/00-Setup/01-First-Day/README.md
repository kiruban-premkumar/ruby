Welcome to Le Wagon!

Congratulations for setting up your workstation, you now have
all the tools you need to successfully work your way through the bootcamp during these 9 weeks. Remember,
your new toolbox is:

- **Sublime Text**, where you'll write Ruby code (and more!)
- The **Terminal**, also called a shell, where you'll type commands. When you see a `$` in the exercises, it means that you'll have to type that command
- **git**, the version control tool you'll use to watch your code, snapshot, and thus submit your attempts to Kitt!

Glad you are here, now start with the first exercise, **Submit your code**!