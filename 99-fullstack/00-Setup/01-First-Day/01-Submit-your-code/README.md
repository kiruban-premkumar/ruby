## Background & Objectives

* Understand the structure of an exercise's repo, and the link to your Github account.
* Learn to test your code locally before submitting your solution.
* Learn the basic git commands to commit modifications to your code and push them to Github.
* Discover the features of the Kitt platform.

## Specs

Open the `lib/wagon_start.rb` file. That's where you'll need to edit

* Make the `wagon_start` method return `'That's how it starts'`
* In the terminal, run `rake` and make sure if you're 100% green + have a good style.
* Commit your changes, and push them.
* Come back to Kitt, and refresh the page. You should see your solution on the right

When you're done you can look at the links below.

## Tips & Resources

* [git cheat sheet](http://rogerdudler.github.io/git-guide/files/git_cheat_sheet.pdf)
* [interactive cheat sheet](http://www.ndpsoftware.com/git-cheatsheet.html)
* Watch [this TED conference](http://www.ted.com/talks/clay_shirky_how_the_internet_will_one_day_transform_government.html) on how to use git/Github for usual projects (non-dev)
