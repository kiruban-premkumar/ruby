## Background & Objectives

During these 9 weeks, you'll work in pairs or groups of 3-4. Everyday, a new buddy is assigned to you, following an algorithm of [round robin tournament](http://en.wikipedia.org/wiki/Round-robin_tournament) implemented in this [gem](https://github.com/ssaunier/round_robin_tournament).

Before starting a new challenge, **make sure that you explain to each other what you've understood of it** and **before** starting to write a single line of code. Then you can pair program, or work side by side, checking up every 10 minutes or so to help one another.

Working in pairs is a common practice of Software Engineering. The idea is that if you work alone and you get stuck, you can loose several hours digging in the issue, whereas a fresh pair of eyes would find the problem within seconds. Don't underestimate that!

## Specs

Make sure that the method `today_my_buddys_github_nickname_is` does **return** (not **puts**!) the github username of your buddy.
