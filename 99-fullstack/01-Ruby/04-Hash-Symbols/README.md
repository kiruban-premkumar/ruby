#### `01-Modeling-with-Hash`

The important challenge of the day. Implement methods counting the number of calories in a McDonald order, making use of hashes of course!

#### `02-On-symbols`
An original challenge to make you think about symbols and strings. You will have to implement some "quizz methods". Each time you hesitate between a string and a symbol, ask yourself: is it more like a "tag" for my code? or text-data (e.g. user input)?

#### `03-Argument-order-dependency`
In this challenge, you have to re-implement a method `refrain` using a hash argument to break the argument order dependency. We will use a lot of methods taking a hash of options as a last argument in Rails.

#### `04-Array-to-hash`
A brief challenge to implement a method transforming arrays into hashes with indexed keys. This challenge is optional. Don't do it if your don't feel comfortable with hashes and symbols.
