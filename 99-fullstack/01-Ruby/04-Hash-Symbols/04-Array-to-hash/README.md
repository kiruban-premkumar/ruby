## Background & Objectives

Small exercises to make you manipulate arrays, hashes and blocks, and consolidate your ruby skills on this !

## Specs

Write a method `array_to_hash` which takes an `Array` as an argument
and return a `Hash` built from this array.

- If no block is given, then the hash keys should just be integer indexes of elements in the array.
- If a block is given, call it passing the array index and use what's returned as the array key.