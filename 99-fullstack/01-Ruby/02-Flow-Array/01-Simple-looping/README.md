## Background & Objectives

Learn the two ways to "loop" in ruby:

- With a `for` loop
- With a `while` loop

## Specs

- Write a method in ruby that computes the sum of the integers between a min value and a max value (min and max included).
- If the min value is greather than the max value, it should return -1
- You should at least implement the two simple versions, using `for..end` and `while..end` structures.

## Tips & Resources

- [ruby loops](http://www.tutorialspoint.com/ruby/ruby_loops.htm)
- In Ruby, using an `each` iterator is considered more idiomatic than a `for` loop when you want to iterate through a collection. We will see that in the next days.

## Learning Badges

- What's the difference between using a `while..end` and `for..end` structures ?
