describe "My example group" do
  let(:one) { 1 }

  def two
    2
  end

  before { @another_one = 1 }

  it "has a passing expectation" do
    expect(one).to eq(@another_one)
  end

  it "has a failing expectation" do
    expect(one).to eq(two)
  end
end
