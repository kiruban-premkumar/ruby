$examples = []

def describe(string, &block)
  if block_given?
    eg = ExampleGroup.new string
    eg.instance_eval &block
    $examples << eg
  end
end

at_exit { p $examples.map(&:run) }
