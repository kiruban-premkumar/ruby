class ExampleGroup
  def initialize(name)
    @name = name
    @examples = {}
  end

  def let(name, &block)
    (class << self; self; end).send(:define_method, name) do
      var_name = "@#{name}"
      instance_variable_get(var_name) || instance_variable_set(var_name, block.call)
    end
  end

  def before(&block)
    instance_eval &block
  end

  def it(name, &block)
    @examples[name] = block
  end

  def run
    @examples.inject({}) do |failures, pair|
      name, block = pair[0], pair[1]
      begin
        block.call
      rescue => e
        failures[name] = e.message
      end
      failures
    end
  end

  def expect(object)
    Expectation.new(object)
  end
end

class Expectation
  def initialize(target)
    @target = target
  end

  def to(matcher)
    raise matcher.failure_message unless matcher.matches?(@target)
  end
end

class EqualityMatcher
  def initialize(expected)
    @expected = expected
  end

  def matches?(actual)
    @actual = actual
    @actual == @expected
  end

  def failure_message
    "Expected #{@expected}, got #{@actual}"
  end

  module MatcherMethods
    def eq(expected)
      EqualityMatcher.new expected
    end
  end
end

ExampleGroup.send :include, EqualityMatcher::MatcherMethods

$examples = []
def describe(string, &block)
  if block_given?
    eg = ExampleGroup.new string
    eg.instance_eval &block
    $examples << eg
  end
end

at_exit { p $examples.map(&:run) }

describe "My example group" do
  let(:one) { 1 }

  def two
    2
  end

  before { @another_one = 1 }

  it "has a passing expectation" do
    expect(one).to eq(@another_one)
  end

  it "has a failing expectation" do
    expect(one).to eq(two)
  end
end
