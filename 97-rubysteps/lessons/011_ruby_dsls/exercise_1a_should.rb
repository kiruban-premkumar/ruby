class Object
  def should(matcher)
    raise "Failed expectation" unless matcher.matches?(self)
  end
end

class EqualityMatcher
  def initialize(expected)
    @expected = expected
  end

  def matches?(actual)
    actual == @expected
  end
end

def eq(expected)
  EqualityMatcher.new expected
end

"foo".should eq("foo")
"foo".should eq("bar") # raises exception
