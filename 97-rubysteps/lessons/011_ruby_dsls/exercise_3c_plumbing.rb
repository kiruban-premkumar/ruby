class ExampleGroup
  def initialize(name)
    @name = name
    @examples = []
    @lets = []
  end

  def let(name, &block)
    @lets << {name: name, block: block}
  end

  def before(&block)
    block.call
  end

  def it(name, &block)
    @examples << Example.new(name, block)
  end

  def run
    @examples.inject({}) do |failures, example|
      begin
        example.define_lets(@lets)
        example.run
      rescue => e
        failures[example.name] = e.message
      end
      failures
    end
  end
end

class Example
  attr_reader :name

  def initialize(name, block)
    @name = name
    @block = block
  end

  def expect(object)
    Expectation.new(object)
  end

  def run
    instance_eval &@block
  end

  def define_lets(lets)
    lets.each {|l| let l[:name], l[:block] }
  end

  private
  def let(name, block)
    (class << self; self; end).send(:define_method, name) do
      var_name = "@#{name}"
      instance_variable_get(var_name) || instance_variable_set(var_name, block.call)
    end
  end
end

class Expectation
  def initialize(target)
    @target = target
  end

  def to(matcher)
    raise matcher.failure_message unless matcher.matches?(@target)
  end
end

class EqualityMatcher
  def initialize(expected)
    @expected = expected
  end

  def matches?(actual)
    @actual = actual
    @actual == @expected
  end

  def failure_message
    "Expected #{@expected}, got #{@actual}"
  end

  module MatcherMethods
    def eq(expected)
      EqualityMatcher.new expected
    end
  end
end

Example.send :include, EqualityMatcher::MatcherMethods
