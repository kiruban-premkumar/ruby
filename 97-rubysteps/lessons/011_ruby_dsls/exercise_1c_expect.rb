class Expectation
  def initialize(target)
    @target = target
  end

  def to(matcher)
    raise matcher.failure_message unless matcher.matches?(@target)
  end
end

class EqualityMatcher
  def initialize(expected)
    @expected = expected
  end

  def matches?(actual)
    @actual = actual
    @actual == @expected
  end

  def failure_message
    "Expected #{@expected}, got #{@actual}"
  end
end

def eq(expected)
  EqualityMatcher.new expected
end

def expect(object)
  Expectation.new(object)
end

expect("foo").to eq("foo")
expect("foo").to eq("bar") # raises exception
