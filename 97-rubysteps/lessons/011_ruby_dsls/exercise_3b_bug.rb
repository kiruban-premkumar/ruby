describe "My example group" do
  let(:one) { "1" }

  it "has a passing expectation" do
    expect(one).to eq("1")
    one.succ!
  end

  it "has a failing expectation" do
    expect(one).to eq("1")
  end
end
