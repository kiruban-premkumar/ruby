class Object
  def should(matcher)
    raise matcher.failure_message unless matcher.matches?(self)
  end
end

class EqualityMatcher
  def initialize(expected)
    @expected = expected
  end

  def matches?(actual)
    @actual = actual
    @actual == @expected
  end

  def failure_message
    "Expected #{@expected}, got #{@actual}"
  end
end

def eq(expected)
  EqualityMatcher.new expected
end

"foo".should eq("foo")
"foo".should eq("bar") # raises exception
