# Lesson 011 - Ruby - DSLs

### Running the exercises

See the top-level README for how to run the exercises. This lesson's exercises
are in the `lessons/011_ruby_dsls` directory.

If you run into problems, it may be because your vagrant box is out of date. You
can run `vagrant box outdated` to check, and then `vagrant box update` to update
the image. You then need to destroy your local box and restart it, with `vagrant
destroy` and `vagrant up`.

--------

# DSLs

Ruby's flexible syntax allows you to express your ideas in code almost however
you like. This makes it especially well-suited for creating domain-specific
languages.

To tell you the truth, I'm not quite sure where to draw the line between an
expressive library and a DSL. At least when working with an "internal DSL" - one
implemented in the host language - I don't think the line matters all that
much. Perhaps the best way to understand what makes a DSL is to look at some
popular ones in Ruby.

* ActiveRecord provides a DSL for managing validations, associations, and
  database interaction
* RSpec provides a DSL for defining automated tests
* Chef provides a DSL for managing server infrastructure
* Rake provides a DSL for managing build dependencies

It's easier for me to identify a DSL than to define it - "I know a DSL when I
see it." If I had to list some defining characteristics, I'd say a DSL commonly
has one or more of the following:

* a cohesive library
* declarative style
* use of blocks
* use of metaprogramming
* runs in its own context

Let's start off by taking a look at RSpec's DSL syntax. People can find it
confusing, but once you understand how it works you may find it rather elegant.

I'm not going to look at the exact implementation today. Instead, we're just
going to look at how you could implement something like RSpec, to get an idea of
how it works. You'll probably learn a couple tricks along the way.

--------

## Exercise 1 - Implementing the `should` expectation matcher

I'm going to take it old school for a moment and show you RSpec's old syntax,
because it's what I originally fell in love with and it shows off some
interesting stuff. When we're done with that, we'll look at how to update our
code to use the current syntax.

Note: This isn't the oooold syntax. There was another syntax that was
`more.like.this` that I was never a fan of.

Here's an example of the syntax we'll implement:

```
actual.should matcher(expected)
```

It means that we expect something to be true of the actual object. The matcher
defines what exactly we expect.

The syntax works for two reasons. First, we extend `Object` to have the
method `#should`. Second, we pass a polymorphic matcher to `#should`.

Take a look inside `exercise_1a_should.rb` for an initial implementation. How
about that? One of the most mystifying parts of RSpec, explained in under 25
lines of code :)

Of course, now that we have a basic structure in place, we can make the behavior
more sophisticated. We can start by adding a more helpful failure message, as
you see in `exercise_1b_failure.rb`.

Because the matcher is polymorphic, you can add your own. Why not try adding one
for string inclusion, or closeness of numbers, or boolean messages? For fun, try
implementing the `#should_not` matcher.

We can change the code to use the more up-to-date RSpec syntax: the only
requirement is that we have an object which can handle a matcher. Take a look at
`exercise_1c_expect.rb` to see how you could do this. Notice that it's a really
trivial change to the code - the main structure stays the same, we've just given
it a different interface.

I'd say this code on its own represents a small DSL. It is an expressive library
built with testing in mind, easily extensible, and runs in its own context. I
haven't shown the context, but let's look at that in the next exercise.

## Exercise 2 - Building with blocks

The little DSL we've built so far may not seem like much to you, but after
building the equivalent of that in a dozen different languages I don't get
snobbish about test frameworks. I use tools and frameworks to help me do that
sort of stuff faster, and to be able to manage complexity better.

In this exercise, I want to demystify a bit more of RSpec's syntax. I find RSpec
very simple and elegant, but if you are still getting a grip on certain parts of
Ruby you might find it much more challenging.

We already looked at the basics of `expect`, so here I want to look at the DSL
used to define the structure of executable specifications. It revolves around
blocks - everything important is a method call that uses blocks, and then a
runner executes those blocks as needed. Take a look inside
`exercise_2_blocks.rb` for an example of implementing an RSpec-style
syntax. I've chosen a fairly full-featured spec, so you can see all the main
pieces of the puzzle.

If you're not that familiar with Ruby, some of this might look like magic to
you. The key thing to understand when looking at this code is what
`#instance_eval` does - it evaluates a block of code in the context of the
target object. In other words, `self` in that block of code will be the object
that you called `#instance_eval` on. If you just call the block directly, it
evaluates in the context it was defined in.

The `#instance_eval` calls are an example of metaprogramming, because we're
defining code in one context and running it in another one. Another example of
metaprogramming is the calls to `#define_method` as a part of `#let`. Lines 8-11
demonstrate a few common metaprogramming techniques - first it opens the
object's metaclass with `(class << self; self; end)` and then defines a new
instance method on it with `#define_method`. Defining a method on an object's
metaclass creates a "singleton method" - a method which lives only on that
instance of the class. It then dynamically reads a variable of the same name, or
assigns it if necessary.

That's a lot to chew on - but I want you to see how we can implement a pretty
useful test framework in just a little bit of code. That whole file is 100 lines
long, and includes a test framework and example tests. It's easily extendible
with new matchers. Not too shabby.

Of course, it's not perfect. There's actually a bug in it that might be hard to
see. Can you find it? If not, read on to the next exercise.

## Exercise 3 - Porcelain and Plumbing

It can be useful to separate the DSL implementation from the library
implementation. You'll often find this referred to as the porcelain and the
plumbing. The plumbing is a typical library with an expressive API, and the
porcelain provides a nifty interface to it.

In this exercise, I've split the code from exercise 2 into three files. In
`exercise_3a_spec.rb` you'll see the original executable
spec. `exercise_3a_porcelain.rb` is empty, and `exercise_3a_plumbing.rb` has the
framework code. I've just separated the original spec code for now, and created
a place to put the porcelain code. Run it with:

`ruby -I . -r exercise_3a_porcelain -r exercise_3a_plumbing exercise_3a_spec.rb`

(This runs `exercise_3a_spec.rb`, adding the current directory to the require
path and then requiring the porcelain and plumbing files. This lets us write
multiple implementations without ever changing the spec)

Now I want to move stuff over. The basic principle is that anything that
represents the structure and behavior of the library lives in plumbing. Anything
that makes that structure and behavior available via a nice DSL is
porcelain. Using that as my deciding factor, I've split the code up as you see
in `exercise_3b_porcelain.rb` and `exercise_3b_plumbing`. Run it with:

`ruby -I . -r exercise_3b_porcelain -r exercise_3b_plumbing exercise_3a_spec.rb`

I've just done a simple extraction, moving the `describe` method and `at_exit`
hooks out of plumbing. There are a couple more methods I might move over to
porcelain, but I'll highlight a bug first. Run:

`ruby -I . -r exercise_3b_porcelain -r exercise_3b_plumbing exercise_3b_bug.rb`

The test fails, even though it shouldn't. Can you spot the bug? The test runner
doesn't isolate state in between tests.

I'll extract an Example object, which will give me a bit more of a structured
API to drive with the porcelain. Take a look at `exercise_3c_porcelain.rb` and
`exercise_3c_plumbing.rb` to see how I've implemented it. Run the specs with:

`ruby -I . -r exercise_3c_porcelain -r exercise_3c_plumbing exercise_3b_bug.rb`

Now we're starting to see more of the library structure, and which parts support
the DSL. Can you move the methods `#before`, `#let`, and `#it` out of the
`ExampleGroup` class and into the porcelain definition?

Try running our very first example `exercise_3a_spec.rb` and you'll see that it
fails. Why? How can you make it pass? What changes do you need to make to the
structure of the library?

--------

# Wrap up

In this lesson, I've given you an introduction to implementing DSLs in Ruby. We
looked at how to enable an RSpec-like syntax, and some of the common
metaprogramming techniques you might use to implement it. If you're brand-new to
Ruby and this is all way over your heard, don't worry - it's some of the most
complex stuff in Ruby, and by working to understand it now you'll be able to
grok other DSL-type code.

I haven't gone into too much detail about how and when to use DSLs. I'll cover
that more in future lessons. For now, I just want to give you an idea of what
they are and how to implement them in Ruby. Hopefully some things start making a
little more sense to you from now on!

--------

## References

* [Domain-Specific Languages](http://c2.com/cgi/wiki?DomainSpecificLanguage)
* [ActiveRecord validations methods](http://api.rubyonrails.org/classes/ActiveRecord/Validations/ClassMethods.html)
* [RSpec DSL](http://rubydoc.info/gems/rspec-core/RSpec/Core/DSL)
* [Chef recipe DSL](http://docs.getchef.com/dsl_recipe.html)
* [Rake DSL](http://docs.ruby-lang.org/en/2.1.0/Rake/DSL.html)
* [instance_eval](http://www.ruby-doc.org/core-2.1.2/BasicObject.html#method-i-instance_eval)
* [at_exit](http://www.ruby-doc.org/core-2.1.2/Kernel.html#method-i-at_exit)
* [to grok](http://c2.com/cgi/wiki?ToGrok)
