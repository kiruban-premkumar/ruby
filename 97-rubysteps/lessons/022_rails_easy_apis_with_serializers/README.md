# Lesson 022 - Rails - Easy APIs with Serializers

Rails has been a great tool for quickly building APIs for a long time, but the
`active_model_serializers` library really makes things easy. It provides a new
mechanism in the standard Rails MVC architecture that encapsulates the
responsibility of serializing objects as part of the render cycle.

## Exercise 1 - Easy APIs without serializers :)

Let's start off by looking at an example without serializers. You can always
simply render a response directly from your controller, whether it's plain text
or JSON or XML. Take a look at
`demo_app/app/controllers/easy_responses_controller.rb` for a quick example. The
`index` action just renders a plain-text response: sometimes that's all you
need! I've used this sort of response to provide simple "ping" endpoints where
we want to know whether the app is running, or if some condition is true. You
can even just use an HTTP response code if you don't need any data. Check it out
by running the Rails server and visiting `http://localhost:3000/easy_responses`
in your browser.

The `show` action shows an example of constructing a JSON response
manually. Rails will automatically convert any object into JSON if it has a
`#to_json` method, which `Hash` does. In this case, the `show` action follows
the "echo" pattern: it essentially echoes back the input received via URL
parameters, along with some extra information - in this case, the current time
and a brief note.

You can see right off the bat that you can build APIs with very little effort.
If you can create a controller and build a hash, you can make an API. Why spend
time building a database-backed model if you don't need it?

## Exercise 2 - Rendering models

Of course, you will frequently want to expose your application's data via an
API. Rails already supports this by default. Take a look at
`demo_app/app/controllers/easy_models_controller.rb` where the `render json:
build_model` call results in the returned object being rendered to json via a
call to `#as_json`.

You'll notice that Rails automatically renders each of the model's fields as
part of the response. This poses an interesting question - what if we want to
include more data, or hide some data like the `secret_field` value? You can
customize the information returned by defining `#as_json`. Uncomment the
`#as_json` method definition in `demo_app/app/models/easy_model.rb` to see how
the response changes. `#as_json` returns a Hash representing the object to be
converted to json. `#to_json` uses that to convert it to a string.

This works, but it can get messy when you want to convert objects to json in
different contexts. An admin should be able to see all of the information,
whereas other users might only be able to see a part of it. In the past I'd
solved that by creating special methods such as `#as_json_admin` and explicitly
calling it from the controller... but now we have presentation-specific concerns
leaking into the model.

## Exercise 3 - Enter ActiveModel::Serializers

`ActiveModel::Serializers` let you define an explicit view layer for your
APIs. Instead of building up JSON in a controller, or specialized methods in the
model, you define serializers which render the information you need.

Take a look at `demo_app/app/controllers/more_easy_models_controller.rb` where
you'll see an example of a serializer being used. The actual serializer
definition lives in `demo_app/app/serializers/my_easy_model_serializer.rb`. The
serializer explicitly defines attributes to include in the JSON output - and as
you can see, it's possible to define methods on the serializer so that you don't
have to pollute your model with presentation logic.

See it in action by visiting `http://localhost:3000/more_easy_models/74` in your
browser. You can trigger the `AdminEasyModelSerializer` by adding `?admin=true`
to the URL, this will reveal the `secret_value`.

Serializers actually give us two different angles for decoupling presentation
logic from the model layer. We can define multiple serializers for a single
object type, as I've done in this exercise. We can also use the same serializer
for a totally different type of model! `ActiveModel::Serializers` will use
convention over configuration to automatically determine which serializer to use
for a particular model, but we can explicitly set it as I've done in this
example. As long as you define an object that has the same interface that the
serializer expects, it works just fine. Duck-typing ftw! Try adding a new model
and controller, and using one of the existing serializers to render it.

## Wrap up

Serializers work great as a means of decoupling presentation logic from
models. Because they introduce a new mechanism, you can use serializers without
a model at all! This comes in useful either when you need to provide a
lightweight API endpoint, or even for prototyping. You can define a set of
controllers and serializers, not backed by any model, and build a client that
works with that API. Then you can iteratively implement models and plug them in,
without breaking API clients.

Take some time to read over the documentation for
`ActiveModel::Serializers`. There really isn't much to it, but it does offer
some nifty features like associations. You'll be seeing a lot more of
serializers in future Rails lessons. For now, play around with them and see how
easily they enable you to create APIs in Rails.

## References
* [ActiveModel::Serializers](https://github.com/rails-api/active_model_serializers)
