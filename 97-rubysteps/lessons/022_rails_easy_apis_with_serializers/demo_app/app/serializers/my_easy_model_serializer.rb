class MyEasyModelSerializer < ActiveModel::Serializer
  attributes :name, :secret_value, :timestamp

  def secret_value
    object.secret_value.gsub(/./, '*')
  end

  def timestamp
    Time.now.to_s(:short)
  end
end
