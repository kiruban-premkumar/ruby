class EasyModelsController < ApplicationController
  def show
    # we'll just construct an object in-memory so we don't have to worry
    # about seeding the database
    render json: build_model
  end

  private
  def build_model
    now = Time.now
    EasyModel.new name: 'an easy model', created_at: now, updated_at: now, id: params[:id], secret_value: 'hide me'
  end
end
