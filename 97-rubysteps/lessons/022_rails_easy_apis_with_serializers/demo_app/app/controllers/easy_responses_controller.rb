class EasyResponsesController < ApplicationController
  def index
    render text: 'Sometimes a simple text response is all it takes!'
  end

  def show
    render json: {
      echo: params[:id], now: Time.now, note: 'And other times you want more structure'
    }
  end
end
