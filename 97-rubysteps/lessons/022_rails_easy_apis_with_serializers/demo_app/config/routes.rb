Rails.application.routes.draw do
  resources :easy_responses
  resources :easy_models
  resources :more_easy_models
end
