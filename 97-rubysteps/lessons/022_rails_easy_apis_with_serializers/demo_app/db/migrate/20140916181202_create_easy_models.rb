class CreateEasyModels < ActiveRecord::Migration
  def change
    create_table :easy_models do |t|
      t.string :name
      t.string :secret_value

      t.timestamps
    end
  end
end
