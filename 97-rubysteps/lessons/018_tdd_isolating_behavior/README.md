# Lesson 018 - TDD - Isolating Behavior

### Running the exercises

See the top-level README for how to run the exercises. This lesson's exercises
are in the `lessons/018_tdd_isolating_behavior` directory.

If you run into problems, it may be because your vagrant box is out of date. You
can run `vagrant box outdated` to check, and then `vagrant box update` to update
the image. You then need to destroy your local box and restart it, with `vagrant
destroy` and `vagrant up`.

--------

# Isolating Behavior

You know by now that you can organize your code around responsibilities,
creating classes and modules to represent important behavior. Modules provide
some unique challenges - you can't instantiate them, and they typically contain
only part of the behavior you ultimately want to test. You run the risk of
inadvertently coupling modules to one another, or to a particular class
implementation via dependencies and instance variables.

My rule of thumb when working with modules: don't reference instance
variables. You can't avoid some form of coupling between the module and some
other component. The module depends on an external interface, or an external
component depends on the module. Coupling to an interface rather than an
instance variable gives you much more flexibility. Any object that implements
that interface will suffice. Modules mix in to classes and so can access an
instance variable, but that introduces tight coupling between the module and
that class. For the module to work, an object would need to pre-calculate the
value and store it in the instance variable. Coupling to an interface affords us
all the benefits of polymorphism, and saves us a character of typing to boot.

I have run into trouble when I've let too many modules know about one another. I
have learned to separate an object's runtime composition from its syntactic
implementation. You can design each module independently and compose them to
create a flexible, powerful object at runtime. Ruby gives you the best of both
worlds with expressive modular code.

--------

## Exercise 1 - Discovering a contract

When designing around modules, we need to consider the interfaces that interact
with a module and those that it interacts with. A module can call methods on an
object, or an object can call methods from the module, or both. Some other
object might send messages to an object, which responds to it with methods
implemented in modules.

Let's consider the built-in Ruby module `Enumerable`. The documentation shows us
that we need to implement `#each`, but could we figure that out through code? We
know that if we include the module it gives us certain methods like `#map` and
`#select`, so if we call one of those methods then we should get a
`NoMethodError` from the object, because it's missing `#each` which `Enumerable`
needs us to implement.  Check out `exercise_1a_failing_spec.rb` where you'll see
a spec for a class that includes the `Enumerable` module.

Now that we've figured out the missing `#each` method, we can ditch the class
and work directly with the module. Testing a few of the methods lets us prove
the module's basic behavior. See `exercise_1b_passing_spec.rb` for an
example. Remember that when an object extends a module, the module's methods get
mixed in to the object's singleton class.

Try writing similar tests for `Comparable`. How many do you need to write?

--------

## Exercise 2 - Test-driving modules in isolation

I've said before that sloppy module organization can lead to tight coupling
between modules and classes. You can test-drive modules completely independently
from one another. When working with legacy code, you can simulate a test-driven
process with the technique shown in Lesson 010.

Let's take another look at access control. We can test-drive a module that
allows us to define a flexible, extensible authorization framework. In
`exercise_2a_permissions_spec.rb` I've created a test for a module that provides
a set of authorization methods based on a single template method. This follows
the same pattern as the `Enumerable` and `Comparable` modules. Run the spec with
`rspec -f d exercise_2a_permissions_spec.rb` to see a list of all the examples
that get run.

Test-driving a module by including it into a class or extending an object helps
you keep that module independent from any other classes or modules in the
system. You get the test coverage you need for the behavior, and define a module
that depends only on a well-defined interface.

I have shown an example of depending on an instance method, but other dependency
techniques might make more sense in some cases. We can use all the same
techniques on modules as we do on classes. The `Authorization` module depends on
a message `#permissions_for(user)` which you can test-drive in a new module on
your own. Keep in mind that `Authorization` specifies the interface it depends
on. Your new module can implement that interface, but the modules are not
coupled to one another. Neither module has any knowledge of the other beyond the
fact that they use the same protocol.

--------

## Exercise 3 - Contract tests

I first learned about Abstract Test Cases from JB Rainsberger's book *JUnit 4
Recipes* which he later renamed Contract Tests. He uses them to ensure that two
implementations of an interface conform to a contract. We can use RSpec shared
example groups to specify a contract and any high-level behaviors we want. When
test-driving new implementations, we simply include the shared example group and
our tests will automatically validate that it adheres to the contract.

I've extracted a shared example group from the previous test and put it in
`exercise_3a_contract_spec.rb`. The test for the `Authorization` module uses
RSpec's `it_behaves_like` method to include the shared example group. The shared
examples use the template method, which I implemented in the test as well.

Now you can create a new implementation that adheres to the contract. Try a
policy class like `UsernameAccessControl` from Lesson 017, or role-based
access. By including the shared behavior, you automatically validate your new
implementation against the existing contract. You can write tests for more
specific behavior that sit alongside the contract tests. You include the
contract tests with one line of code, and then implement any required template
methods.

--------

# Wrap up

We all aim to write modular code with a low cost of maintenance. Guidelines like
SOLID and JB's 2 rules of simple design help us make and evaluate decisions in
the moment. By using tests to exercise our modular code, we can get quick
feedback on the ideas. If you write tests that exercise a specific set of
behaviors in an isolated context, the component under test will end up cohesive
and decoupled. The tests provide lasting validation of the behavior. When you
need to make two implementations of the same interface, you can extract shared
tests and use them for all implementations. Writing tests that exercise objects
in isolated contexts helps you create interfaces that snap together cohesive,
loosely coupled implementations.

--------

## References

* [Abstract Test Cases](http://c2.com/cgi/wiki?AbstractTestCases)
* [Contract Tests](http://blog.thecodewhisperer.com/2010/09/23/stub-your-data-access-layer-it-wont-hurt) - JB Rainsberger
* [RSpec shared examples](https://www.relishapp.com/rspec/rspec-core/docs/example-groups/shared-examples)
