class DiscoveringEnumerable
  include Enumerable
end

describe DiscoveringEnumerable do
  it "maps objects" do
    expect(DiscoveringEnumerable.new.map {|i| i * 2 }).to eq([2, '**', 6])
  end
end
