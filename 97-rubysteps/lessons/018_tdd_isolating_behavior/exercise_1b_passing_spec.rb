describe Enumerable do
  it "maps objects" do
    object = Object.new
    object.extend Enumerable
    allow(object).to receive(:each).and_yield(1).and_yield('*').and_yield(3)

    expect(object.map {|i| i * 2 }).to eq([2, '**', 6])
  end
end
