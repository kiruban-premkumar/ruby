module Authorization
  def can_show?(user)
    permissions_for(user).include? :show
  end

  def can_edit?(user)
    permissions_for(user).include? :edit
  end

  def can_update?(user)
    permissions_for(user).include? :update
  end

  def can_destroy?(user)
    permissions_for(user).include? :destroy
  end
end

describe Authorization do
  let(:object) { Object.new.tap {|o| o.extend Authorization } }

  it "can show a user with proper permissions" do
    expect(object).to receive(:permissions_for).with(:user).and_return [:show]
    expect(object.can_show?(:user)).to eq(true)
  end

  it "can't show a user without proper permissions" do
    expect(object).to receive(:permissions_for).with(:user).and_return [:edit]
    expect(object.can_show?(:user)).to eq(false)
  end

  # instead of duplicating these tests for each one, we can programmatically
  # generate the tests
  #
  # Be careful with these kinds of tests because they're tricky, and don't
  # overuse them. You can often eliminate duplication in tests with a new
  # abstraction or a custom expectation.
  [:edit, :update, :destroy].each do |action|
    it "can #{action} a user with proper permissions" do
      expect(object).to receive(:permissions_for).with(:user).and_return [action]
      expect(object.send("can_#{action}?", :user)).to eq(true)
    end

    it "can't #{action} a user without proper permissions" do
      expect(object).to receive(:permissions_for).with(:user).and_return [:nothing]
      expect(object.send("can_#{action}?", :user)).to eq(false)
    end
  end
end
