# Lesson 012 - Rails - Mini Frameworks

### Running the exercises

See the top-level README for how to run the exercises. This lesson's exercises
are in the `lessons/012_rails_mini_frameworks` directory.

If you run into problems, it may be because your vagrant box is out of date. You
can run `vagrant box outdated` to check, and then `vagrant box update` to update
the image. You then need to destroy your local box and restart it, with `vagrant
destroy` and `vagrant up`.

--------

# Mini Frameworks

As we've seen in previous lessons, Ruby's syntax and dynamic nature allows you
to write highly expressive programs. I don't think there's any better example of
that than Rails. Rails takes advantage of every Ruby trick there is in order to
create the expressive, powerful, extendible framework that we know and love.

Rails works because DHH's embraced Ruby's dynamic nature. Had he stuck to some
of the "common sense" rules of statically-typed languages, Rails would never
have been as beautiful or as popular as it is now.

Within any Ruby application, you have the opportunity to write small amounts of
code that yield big returns. This might come in the form of metaprogamming, or
it might simply be a flexible library - in any case, you will find yourself
commonly writing the same kinds of code over and over in your application. When
you notice yourself doing that, it can be helpful to extract or create a
mini-framework.

I encountered this technique years ago on one of the first Rails apps that I
worked on. The lead programmer on the project fully embraced Ruby's dynamicity
along with Rails' creative use of it. He constantly looked for opportunities to
distill some common pattern in code down to a few lines of metaprogramming. Just
like ActiveRecord, once you understand the basic library calls and patterns, you
are able to use these mini-frameworks to define lots of functionality in no
time.

--------

## Exercise 1 - Basic authentication

We'll start off by creating a simple authentication mechanism. There are plenty
of gems you can use to do authentication in Rails, but I think it's important to
understand the fundamentals of what's going on behind the scenes. Plus I love to
see the look on people's faces when they see how easy it is! Creating a simple
authentication mechanism will be a good starting point for introducing more
sophisticated framework elements.

Rails provides a simple way to hook into HTTP's authentication mechanism via the
`authenticate_or_request_with_http_basic` controller method. It yields
two values to a block - the username and password provided by the
client. Typically you'll call it as part of a `before_filter` call, as you see
in `mini_frameworks/app/controllers/application_controller.rb`.

If no user exists with the provided username and password combination, the block
returns `nil` and `authenticate_or_request_with_http_basic` returns a "401
Unauthorized" HTTP response.

You can load the seed data by switching to the `mini_frameworks` directory where
the demo Rails app is, and then running `rake db:create db:migrate db:seed`.

Start up the server with `rails s` and then visit the following URL in your
browser: `http://localhost:3000/users/me`. First try entering some random
credentials - they won't work, and it will prompt you again for a working
username and password combination. Now enter 'test-user' as the username and
't3hp4ssw0rd' as the password - you should see the text: "Hello, test-user!".

That's really all there is to adding authentication to a Rails app. It simply
uses the basic authentication mechanism built right into the HTTP standard. Of
course, when you access something in the browser it's the 90s-looking HTTP AUTH
window. For quickly locking down an app while you develop, or adding
authentication to an API, this approach works great.

One problem with the authentication right now is that it stores passwords in the
database in plain-text - can you change the User class so that it encrypts
passwords instead?

--------

## Exercise 2 - Adding an authorization framework

Authentication is critical in most applications, but it's only one piece of the
puzzle. Authentication helps us verify that someone is who we think they are -
at least to the degree that we trust the security of our application and the
user's ability to protect their own login credentials.

Once someone has authenticated with the system, we often need to expose
behaviors to them based on a set of permissions. For example, a common rule in
applications is that people can view and edit their own data but not anything
that belongs to others. Admins might be able to view and edit anything.

Again, there are gems you can install, but I want to show how simply you can add
authorization to any application with a little bit of custom code. I also want
to show you an example of creating mini frameworks within your application. This
type of authorization framework was the first mini framework I ever worked with
inside of a Rails app. It takes advantage of Ruby's dynamic nature, as well as
Rails' notion of convention over configuration.

The basics of the framework are simple: controllers are tied to resources, which
are represented by model objects. The controllers dynamically load the objects
and then check whether the current user has permission to perform that action on
the object.

Take a look inside `mini_frameworks/app/controllers/articles_controller.rb`. It
has one method, which shows you the basic structure of the framework
code. Typically you'll extract frameworks from working code. You start off by
writing the code you need to get the job done, then refactor to make it dynamic
and reusable. The `show` method is a simple example of the authorization pattern
we're using. It simply finds the object, asks if the current user has permission
to show it, and then renders it if so.

Visit the following URL in your browser: `http://localhost:3000/articles/hello`
It should render an article as JSON, as long as you are logged in. Now go to
`http://localhost:3000/articles/hello-draft` and you'll see the text
"Unauthorized". If you look inside of `mini_frameworks/app/models/article.rb`
you'll see the method definition of `#viewable_by?(u)` which checks if the
article is published. If it's not published, it checks if the user is an admin
or is the user who wrote the article.

You can authenticate as an admin or the author of the article by embedding your
credentials directly in the url, as in:
`http://edgar:r4v3n@localhost:3000/articles/hello-draft`

Now take a look at `mini_frameworks/app/controllers/authorized_controller.rb`
for an example implementation of a more dynamic framework. I have implemented a
couple of the common controller methods you might use. Try your hand at
implementing the rest.

You can hit the `create` action via the command line, using curl:
`curl 'http://edgar:r4v3n@localhost:3000/articles/' -d 'article[title]=Hello Chicken', -d 'article[slug]=new-hello'`

Then you can try viewing your newly-created article via:
`curl 'http://edgar:r4v3n@localhost:3000/articles/new-hello'`
and see an authentication failure when logging in as test-user:
`curl -i 'http://test-user:t3hp4ssw0rd@localhost:3000/articles/new-hello'`

You can also delete the `show` method in `ArticlesController` so it uses the
framework version from `AuthorizedController`. We have to implement the
`resource_by_id` method on `ArticlesController` since it's different from the
usual `ActiveRecord::Base.find` call that the framework expects. Once you define
that method (look in the original `show` method to figure out how), everything
else works as you'd expect. This demonstrates a basic principle of
frameworks. The framework has hook points that it expects you to implement, such
as the `#viewable_by?` and `#creatable_by?` methods, that it calls as a part of
its processing. If you follow the conventions, you might not have to implement
any hook points at all. When you need to deviate from the conventions, there are
often small hook points that you can implement that will get you the behavior
you need.

Now that you have this framework in place, try adding new models and
controllers. You can achieve fairly sophisticated authorization behavior simply
by implementing the `#viewable_by?` etc methods on your model objects.

--------

# Wrap up

In this lesson, I've demonstrated how to use mini frameworks to simplify your
Rails code - or any Ruby application, for that matter. Because Rails has so many
conventions to begin with, it's easy to create mini frameworks within your Rails
application. You benefit from the many conventions that it already uses, and you
can plug into those. Plus, other developers already understand the conventions.

I admit that this kind of style can feel a bit like magic at first. However,
once you understand the basic framework structure, it's easy to pick up
on. Anyone can understand the convention and begin adding new authorization
checks by implementing the appropriate methods. This consistency leads to fewer
bugs, and easier-to-maintain code.

We will be exploring this technique in more depth in future lessons. The key
takeaway for now is to look for opportunities to use simple dynamic tricks to
make your code smaller and more expressive. If you embrace this practice, you'll
be able to build modular applications that are easy to understand and change.

--------

## References

* [HTTP basic auth](https://en.wikipedia.org/wiki/Basic_access_authentication)
* [Convention over configuration](http://c2.com/cgi/wiki?ConventionOverConfiguration)
* [Hollywood principle](http://c2.com/cgi/wiki?HollywoodPrinciple)
* [Template Method Pattern](http://c2.com/cgi/wiki?TemplateMethodPattern)
