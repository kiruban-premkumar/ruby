class Article < ActiveRecord::Base
  belongs_to :user

  def viewable_by?(u)
    return true if published_at?

    u.admin? || user == u
  end

  def creatable_by?(u)
    u.admin? || u.writer?
  end
end
