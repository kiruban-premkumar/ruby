class User < ActiveRecord::Base
  def admin?
    username == 'admin'
  end

  def writer?
    %w(edgar walt ernest).include? username
  end
end
