class UsersController < ApplicationController
  def me
    render text: "Hello, #{current_user.username}"
  end
end
