module AuthorizedController
  def resource_by_id
    resource_class.find params[:id]
  end

  def resource_class
    self.class.name.gsub('Controller', '').singularize.constantize
  end

  def resource_params
    params.require resource_class.to_s.underscore.to_sym
  end

  def show
    resource = resource_by_id

    if resource.viewable_by?(current_user)
      render json: resource
    else
      render text: 'Unauthorized', status: :unauthorized
    end
  end

  def create
    resource = resource_class.new resource_params

    if resource.creatable_by?(current_user)
      resource.user = current_user
      resource.save!
      render json: resource
    else
      render text: 'Unauthorized', status: :unauthorized
    end
  end
end
