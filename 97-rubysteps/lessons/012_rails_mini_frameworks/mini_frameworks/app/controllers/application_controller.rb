class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :basic_http_authentication

  def basic_http_authentication
    authenticate_or_request_with_http_basic do |username, password|
      self.current_user = User.find_by_username_and_password(username, password)
    end
  end

  def current_user=(user)
    @current_user = user
  end

  def current_user
    @current_user
  end
  helper_method :current_user
end
