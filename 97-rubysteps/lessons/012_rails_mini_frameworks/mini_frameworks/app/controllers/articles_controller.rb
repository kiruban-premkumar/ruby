class ArticlesController < ApplicationController
  skip_before_filter :verify_authenticity_token # easy command-line access

  include AuthorizedController

  # overrides AuthorizedController#show
  def show
    article = Article.find_by_slug! params[:id]

    if article.viewable_by?(current_user)
      render json: article
    else
      render text: 'Unauthorized', status: :unauthorized
    end
  end

  def resource_params
    super.permit :title, :slug
  end
end
