class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :title, null: false
      t.string :slug, null: false
      t.integer :user_id, null: false
      t.datetime :published_at

      t.timestamps
    end
  end
end
