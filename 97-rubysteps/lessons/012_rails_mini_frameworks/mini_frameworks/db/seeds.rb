test = User.create! username: 'test-user', password: 't3hp4ssw0rd'
writer = User.create! username: 'edgar', password: 'r4v3n'
admin = User.create! username: 'admin', password: 'd0ntr00tm3br0'

Article.create! title: 'Hello World (draft)', slug: 'hello-draft', user: writer
Article.create! title: 'Hello World', slug: 'hello', user: writer, published_at: Time.now
