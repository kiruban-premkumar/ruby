# Lesson 014 - OOP - APIs

### Running the exercises

See the top-level README for how to run the exercises. This lesson's exercises
are in the `lessons/014_oop_apis` directory.

If you run into problems, it may be because your vagrant box is out of date. You
can run `vagrant box outdated` to check, and then `vagrant box update` to update
the image. You then need to destroy your local box and restart it, with `vagrant
destroy` and `vagrant up`.

--------

# OOP - APIs

Object-oriented applications can contain a great deal of functionality. Some of
this functionality represents high level business concepts such as "send welcome
notification when someone signs up". Some of the functionality plays a
supporting role, such as "validate a user's email address", or "downcase a
string". Exposing the high level functionality in a consistent way makes the
system's purpose and use clear.

When you don't create a consistent interface for the most important behaviors,
you end up wasting time as you repeat the same basic procedures over and
over. You miss out on the opportunity to understand the underlying business
problem, and to communicate it to other people in your organization. You miss
out on the opportunity to deliver business value by automating a repetitive task
and making it easily and obviously accessible.

You don't have to create a huge interface up front. You don't have to create one
up front at all, in fact. You can grow an interface as you identify repetitive
behaviors.

You can evolve an interface as you develop the application, whether you're
starting from scratch or taking over a legacy code base for the first time. In
the following exercises, I will share patterns for exposing application
functionality and talk about the possible benefits and drawbacks of each.

--------

## Exercise 1 - Network of objects

In this style, you interact with objects directly. You pull up a console and
require the files you need, or run an application runner, or write scripts. The
library might have a consistent structure and make use of
convention-over-configuration for dynamic behavior, or it might be completely
ad-hoc. It could follow an MVC style. In any case, you programmatically interact
with the software as a library rather than as an application.

I used this approach in the DailyTracker example in Lesson 006. I created
objects to do the heavy lifting of managing storage, tracking daily notes, and
handling command-line input. I've copied the files
`exercise_1_implementation.rb` and `exercise_1_storyish.rb` from lesson 006 into
this lesson's folder for easy access.

With just a few classes, and only two real use cases so far, it's easy to write
a 10-line script that initializes and coordinates the objects needed to run the
application.

When you have more classes and use cases, it can get tough to identify the core
behaviors based on class and method names alone. In such cases, the following
patterns can help explicitly delineate the top-level business behaviors and
create a consistent interface for invoking them.

--------

## Exercise 2 - Flat application interface

If you want to simply and explicitly communicate the application's core
functionality, you can do so via a flat application object. The application
object provides an interface to the most important behaviors. It can delegate to
a network of objects, coordinating them to perform the desired
functionality. Using this style, when you identify a repetitive task, you can
simply create a method on the application object to make it available.

In `exercise_2_app_object.rb` I've created an application object with methods
for the two main application behaviors. I've updated the handler to use this
application object instead of a DailyTracker object. I've also moved the
application setup into this object. Its interface now reflects the application's
primary behaviors. In addition to this increased clarity, we can write new
clients that programmatically drive this application via its bog-standard Ruby
interface. You can see an example of that in `exercise_2_driver.rb`, which you
can run via command-line ruby or copy and paste into irb. We actually "run" the
application multiple times in that one script, like a test.

You may have noticed that I had to make some small changes to the DailyTracker
implementation. When it loaded its entries from the database, it would add on to
whatever entries it had loaded previously. Creating a programmatic interface to
an application can reveal problems in the initial implementation. In this case,
the way DailyTracker managed state led to a bug in a simple client driver.

The `DailyTrackerApp#run` method provides a way of running the app exactly as if
you were doing it from the command-line. The `report`, `add_food_entry`, and
`usage_notes` methods clearly expose the application's primary behaviors.

Add another entry type. If you did this as part of lesson 006, port your changes
over here and incorporate them into the flat application object. What do you
think of the interface of `DailyTrackerApp`? How could you change it to be even
more explicit and useful? Take a look at the `#add_food_entry` method - it has a
bug if you try to call it directly, rather than invoking it via the input
handler. How could you change it so that calls to
`DailyTrackerApp#add_food_entry` add and save new entries to the tracker
journal? Try changing the driver application to call the behavioral methods
rather than the top-level `#run` method.

The octokit gem from GitHub provides this kind of flat application interface to
their web application. The `Octokit::Client` class includes several different
modules, each of which builds up the flat namespace.

--------

## Exercise 3 - Service layer

You might prefer to break up the core functionality into a layer of service
objects. Each service provides some cohesive subset of the core functionality,
and can be decoupled from other services. You can deploy the services
separately if you want, or just use them as a way of logically grouping
application functionality.

Take a look at `exercise_3_service_objects.rb` for an example of how I split this
application up into services. Again we can drive these objects as seen in
`exercise_3_driver.rb`. I've made one call with the handler using simulated
input, as in the previous exercise. I've followed that up with calls to the
different application services.

This approach can help break up a huge flat application interface. Thinking of
the application's behavior in terms of services helps you draw boundaries around
logic and decouple it from other parts of the application. That alone can make
the system easier to reason about. It also opens up new deployment options. The
`TrackerWriteService` and `TrackerReadService` in this exercise demonstrate the
CQRS architectural pattern, where the read and write portions of a service are
deployed separately to accomodate different scaling requirements.

--------

## Exercise 4 - Command objects

You can also break up functionality in terms of the commands that the software
processes. You create command objects to encapsulate and coordinate each
behavior. Clients build command objects, and execute them directly or possibly
by way of a message bus. In `exercise_4_command_objects.rb` I have changed the
application to use commands. Some of the commands delegate to the same service
objects I created in exercise 3. Other commands call the domain objects
directly. I've done this so you can see that these design styles apply at any
level, and you can plug them together as needed. When I use command objects, I
don't write them at multiple levels of abstraction as I've done here. Try
changing the command objects to use a consistent level of abstraction. Which do
you prefer, and why? `exercise_4_driver.rb` has the driver application.

Command objects make certain kinds of new functionality easy. Because commands
follow a consistent interface, you can introduce new functionality simple by
defining a new command. You can log commands for a simple audit trail. You can
make commands reversible in order to support undo and redo functionality. You
can publish new commands to a bus and let a background server process them. You
can replay commands to test them against a different infrastructure or with
different business rules.

Can you add new commands to the application? How do you feel this style compares
to the ones we've looked at previously?

--------

# Wrap up

In this lesson, I've shown you four different styles for defining an API. I find
them particularly useful when I want to identify and programmatically use the
application, but these patterns can be used at any layer in the application. As
I showed in exercise 4, you can combine the pattern. Each of them is a common
design pattern applied to the application's core behavior, and demonstrates a
different way of interacting with an object-oriented program. Even if you don't
want to programmatically invoke the behavior, creating a clear API helps to make
the core behavior explicit. If you ever take over a project, you might find it
hard to even answer the question "what does the system do?" Designing an API to
make the core functionality explicit can help you as you maintain the system.

In future lessons, we will look at architectural choices based on these
different API styles. We will also look at the underlying design patterns and
see how to use them in other layers of the system.

--------

## References

* [Octokit::Client](http://rubydoc.info/gems/octokit/3.3.0/Octokit/Client) - flat interface
* [Service Layer](http://martinfowler.com/eaaCatalog/serviceLayer.html) - Martin Fowler P of EAA
* [CQRS](http://martinfowler.com/bliki/CQRS.html) - Martin Fowler
* [Command Pattern](http://c2.com/cgi/wiki?CommandPattern) - c2.com
