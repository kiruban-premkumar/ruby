$:.unshift '.' # add current dir to the require path
require 'exercise_3_service_objects'

InputHandler.new.handle ["a", "f", "b", "oatmeal"]
TrackerWriteService.add_food_entry "l", "ham and eggs"
TrackerWriteService.add_food_entry "d", "tuna salad"
TrackerReadService.report Date.today.to_s
ApplicationHelpService.usage_notes
