class Foo
  def my_method
    puts "Calling Foo#my_method"
    puts "Now about to call super...but it will fail (Object)"
    super # NoMethodError
  end
end

class Bar < Foo
  def my_method
    puts "Calling Bar#my_method"
    puts "Now about to call super...and it will work (Foo)"
    super
  end
end

Bar.new.my_method
