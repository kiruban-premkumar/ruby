class LessonList
  include Enumerable

  def initialize(lessons)
    @lessons = lessons
  end

  def each(&block)
    @lessons.each &block
  end
end

lessons = LessonList.new [[:one, true], [:two, true], [:three, false]]

lessons.each {|l| puts "Lesson #{l[0]} is #{'not ' unless l[1]}released" }
lessons.select {|l| l[1] }.map {|l| l[0] }.tap do |public_lessons|
  puts "Public lessons: #{public_lessons.join(', ')}"
end

# Now for your challenge:
# 1) modify LessonList#each to return the lesson labels only, not release status
# 2) include Comparable and implement <=> to make released lessons have a higher
#    sort order than unreleased lessons
# 3) modify LessonList#each to only return lessons where release status is true

# This example shows how exposing a collection via instance method makes it
# easy to break encapsulation. I've used an obvious attr_reader here, but it
# can also happen in less obvious ways, such as being the return value for
# any instance method
class UnencapsulatedLessonList < LessonList
  attr_reader :lessons
end

broken_lessons = UnencapsulatedLessonList.new [[:one, true], [:two, false]]
broken_lessons.lessons << [:pirate, "chicken"]
broken_lessons.each {|l| puts "Lesson: #{l.inspect}" }
