object = Object.new

class Object
  def reopened
    puts "we can re-open the class"
  end
end

module SomeModule
  def mod_method
    puts "We can define a method in a module and include it into the class"
  end
end
Object.include SomeModule

module MyModule
  def singleton_mod_method
    puts "We can define a method in a module and extend the object directly"
  end
end
object.extend MyModule

def object.a_singleton_method
  puts "We can define a method directly on the object"
end

object.reopened
object.mod_method
object.singleton_mod_method
object.a_singleton_method
