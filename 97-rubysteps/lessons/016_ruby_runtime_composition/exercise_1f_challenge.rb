# Create an object which returns the following string for #to_s
#
# When you know how Ruby handles inheritance, you can do some pretty cool things
#
# You do not need to write any new methods. You simply need to get the
# inheritance heirarchy right.

class SomeClass
  def to_s
    "how Ruby handles inheritance"
  end
end

class MyClass
  def to_s
    "#{super} you can do"
  end
end

module SomeModule
  def to_s
    "When you know #{super}"
  end
end

module SomeOtherModule
  def to_s
    "#{super} some pretty cool things"
  end
end
