class Foo
  def to_s
    "Foo(#{super})"
  end
end

class Bar < Foo
  def to_s
    "Bar(#{super})"
  end
end

module IntoBar
  def to_s
    "IntoBar(#{super})"
  end
end
Bar.include IntoBar

module IntoFoo
  def to_s
    "IntoFoo(#{super})"
  end
end
Foo.include IntoFoo

module IntoBar2
  def to_s
    "IntoBar2(#{super})"
  end
end
Bar.include IntoBar2

puts Bar.new
