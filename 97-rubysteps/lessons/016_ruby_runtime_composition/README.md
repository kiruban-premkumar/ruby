# Lesson 016 - Ruby - Runtime Composition

### Running the exercises

See the top-level README for how to run the exercises. This lesson's exercises
are in the `lessons/016_ruby_runtime_composition` directory.

If you run into problems, it may be because your vagrant box is out of date. You
can run `vagrant box outdated` to check, and then `vagrant box update` to update
the image. You then need to destroy your local box and restart it, with `vagrant
destroy` and `vagrant up`.

--------

# Runtime Composition

Ruby lets you compose an object's behavior on the fly. It may start with a
simple class definition, but you can still change the behavior once the object
has been initialized. You can change the class implementation, include a module,
or override methods for just that one instance. You can even handle messages
that were never defined in the first place, via `method_missing`.

These dynamic features give you choices in terms of how you structure your
code. When you notice code duplication, you can extract it to a module in order
to share it between two classes easily. From there, you might further refactor
some of the code into a base class.

You can do a lot more than just organize code neatly though. You can define
powerful frameworks and DSLs. You can create plugin systems for your
applications. You can refactor whole design patterns down to a few lines of code
using metaprogramming techniques. In order to use this power, you need to
understand how to change a Ruby object's behavior at runtime.

--------

## Exercise 1 - Where do methods come from?

When you send a message to an object, the object responds based on its
implementation. A heirarchy of classes and modules define the object's
methods. The object walks the inheritance heirarchy from bottom to top, and when
it finds a method definition that matches the received message, it responds by
executing the method. By default, objects respond to unknown messages by raising
a `NoMethodError`.

Take a look at `exercise_1a_method_definitions.rb`. In it, I've shown different
ways of defining a method on an object. Each one results in the same basic
behavior of making an object respond to a new message. They differ in terms of
where they define the behavior, which comes into play when using inheritance via
the `super` keyword. In `exercise_1b_ancestors.rb` you'll see a class definition
which demonstrates how the inheritance heirarchy works. It starts off with a
simple class definition, and shows how the ancestors change as modules are
included at different points. Notice how Ruby inserts the modules above the
class in the heirarchy. The module sits between the class and its
superclass. Messages to this object will first look at the class definition,
then any included modules, and then superclasses.

Now that you've seen how Ruby defines the inheritance heirarchy, let's take a
look at how to actually use it with `super`. In `exercise_1c_super.rb` you'll
see another class definition with each component defining `#to_s` and calling
`super`. This example illustrates how Ruby walks the inheritance heirarchy when
responding to a message. `super` will look higher in the heirarchy for a
definition. In this example, the `super` call inside of `Foo#to_s` works because
`Object` defines `#to_s`. If no such method exists, it will perform the standard
behavior and raise a `NoMethodError` as in `exercise_1d_missing.rb`.

There's one exception to what we've seen so far, and that's the singleton
class. Every object has a singleton class. Defining a method on an object's
singleton class makes it available to that object only. See
`exercise_1e_singleton.rb` for an example of using a singleton class. Class
methods are actually methods defined on a class object's singleton class.

In `exercise_1f_challenge.rb` I've defined two classes and two modules that can
be combined to print a particular string. Make it work by fixing the inheritance
heirarchy and including any modules at the relevant spots. Follow the
instructions to create an object with the desired behavior.

--------

## Exercise 2 - Getting behavior for free

Ruby's built-in `Enumerable` and `Comparable` modules provide great examples of
how to compose behavior with modules. They each define several general methods
in terms of one specific method, which you implement in your class. It's the
template method pattern, but without using class inheritance.

Using modules like `Enumerable` and `Comparable` lets you introduce new
abstractions that behave similarly to ones we're already familiar with. By
implementing the relevant methods and including these modules, you make your
object usable in polymorphic contexts.

In `exercise_2a_enumerable.rb` I've shown an example of using `Enumerable`. It
demonstrates how to fully encapsulate a collection object in order to make its
enumerative methods available without allowing changes to internal state. The
abstraction provided by `Enumerable` lets us use this object in polymorphic
contexts while fully encapsulating it.

I have left a couple of small challenges in `exercise_2a_enumerable.rb`. After
reading through and running the code, try making the necessary changes.

--------

# Wrap up

I've really just scratched the surface here, because runtime composition is at
the heart of Ruby. When you run a Ruby application, the language literally
changes as the program runs. Code doesn't evaluate until it's been executed for
the first time. This means you can make references to classes and methods that
don't exist - you won't see an error until Ruby actually tries to run that code.

Composing objects at runtime allows you to organize your methods into modules,
and then assemble complete objects with all of the behavior you want. Each
module can define protocols, enabling the object to be used by clients
polymorphically.

In future lessons, we'll look at runtime composition in much more depth. You can
do fancier stuff than I've shown here, but you'll go a long way just by
understanding the basics. Once you understand how Ruby defines an object's
behavior and looks up method implementations in response to messages, you can
make smart decisions about how to structure your code and compose your objects.

--------

## References

* [Enumerable](http://www.ruby-doc.org/core-2.1.2/Enumerable.html)
* [Comparable](http://ruby-doc.org/core-2.1.2/Comparable.html)
* [Object#singleton_class](http://ruby-doc.org/core-2.1.2/Object.html#method-i-singleton_class)
