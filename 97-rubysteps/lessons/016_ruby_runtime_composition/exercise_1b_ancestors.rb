class Foo
end

print "Foo.ancestors: "
p Foo.ancestors

class Bar < Foo
end

print "Bar.ancestors: "
p Bar.ancestors

module IntoBar
end
Bar.include IntoBar

print "Bar.ancestors: "
p Bar.ancestors

module IntoFoo
end
Foo.include IntoFoo

print "Bar.ancestors: "
p Bar.ancestors

module IntoBar2
end
Bar.include IntoBar2

print "Bar.ancestors: "
p Bar.ancestors
