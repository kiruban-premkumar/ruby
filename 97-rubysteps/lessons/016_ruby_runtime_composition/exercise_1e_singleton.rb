o = Object.new

def o.method1
  puts "Defined on o's singleton class"
end

o.singleton_class.class_eval do
  def method2
    puts "Also defined on o's singleton class"
  end
end

o.method1
o.method2
Object.new.method1 rescue puts "method1 NoMethodError"
Object.new.method2 rescue puts "method2 NoMethodError"

print "o.singleton_class.ancestors: "
p o.singleton_class.ancestors
