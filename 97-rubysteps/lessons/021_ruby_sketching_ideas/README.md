# Lesson 021 - Ruby - Sketching Ideas

Ruby has a concise syntax that allows you to represent ideas in code with
minimal "ceremony" - you have very little extraneous syntax getting in the way
of your ideas. Any pseudocode you write probably will not need much tweaking in
order to turn it into syntactically valid Ruby.

One of my favorite programming experiences ever came during a code retreat a few
years ago. I was pairing with Liz Keogh, and she showed me her "sketching"
technique. She had an idea for what she wanted to do with code, and started
writing it down in the text editor. She wasn't very familiar with Ruby, so she
wasn't trying to write valid Ruby, but her "sketch" was so close to valid Ruby
that it only took a couple minutes to fix it and get it working.

I find this to be an incredibly powerful technique. Translating ideas into code
is only one part of programming - understanding which ideas to translate into
code in the first place is the hard part. Once you know the ideas to express,
you need to write your code in a way that the ideas are clear and
understandable. Too many programmers flail around in their code, trying to get
things to "just work" without having a clear understanding of the underlying
ideas.

## Exercise 1 - Expressing an algorithm

For this exercise, think of some process you're familiar with - your routine as
you go to work for the day, the steps involved in cooking your favorite meal, or
manufacturing some widget.

Write out the algorithm in plain English. Which actors are involved? What
information do they need from the environment? What is the over all story arc?

Break the algorithm into its individual steps. Try to make each step independent
of the others. Later steps absolutely may depend on previous steps, but you
should still be able to understand each step independently.

Take some time to make each step clear and unambiguous. Try "acting out" the
algorithm using physical objects. I like using blank index cards. You don't need
to label them, they just serve as a physical token representing the actors and
relationships in your algorithm.

## Exercise 2 - Abstracting up and down

For each step in the algorithm, "abstract down" and provide the details
necessary to carry out that step. Essentially you are writing a new algorithm to
describe a particular step in the original algorithm. This process will reveal
new encapsulation boundaries and relationships. Which details show up in
multiple high-level steps, broken down into their component steps? These are
candidates for instance variables and methods in the final implementation. It
can also reveal inconsistencies and coupling. Are you able to understand one
high-level step in isolation from another? Can you understand it just from
reading the high-level description, or do you need to read the individual
low-level steps to understand the high-level meaning?

Next, "abstract up" - think of how your original algorithm fits into some higher
level process, how it can represent just one step in another process. Once
you've done that, think of a second process that can use your algorithm as a
step. A useful abstraction can be used in multiple unrelated contexts, so this
is a good test to see how well you've abstracted the original algorithm.

## Exercise 3 - Convert it to code

Now it's time to convert your plain English to code. You're not trying to make
things work at this point, you're simply translating your original abstraction
into something Ruby-like. Start by converting each step into a single method
call - which part of the step is the method name itself, and which parts are
arguments to the methods? What duplication do you see in the method names? Do
any possible classes reveal themselves to you?

Again, don't complete an implementation. Just define the API for your
algorithm. Implementing it is usually straightforward enough once you've clearly
defined the steps involved. Take your time making sure that you clearly
represent the core ideas of the algorithm.

## Wrap up

When working on production systems, time constraints can pressure us into being
satisfied by any code that appears to work, even if it obscures key ideas. Over
time, the key ideas get lost and intertwined so that the system becomes
difficult to understand and maintain.

Practice writing algorithms in English so that you can identify the key ideas
and make them explicit. Thinking through programming problems is hard enough
work as it is, and trying to go straight from thinking to code means that you're
multi-tasking on two highly complex problems. If you can separate these two
activities, you can dedicate all of your intelligence to each one. First, you
think through the programming problem and express it in a way that someone else
can understand. Next, you translate it into code that a computer can understand.

Ruby's minimal syntax means that you can go from pseudocode to working code with
minimal effort. Expressing ideas in English helps you identify the core ideas
and then abstract away the details.

If you're just learning to program, build up a library of these pseudocode
algorithms. They will provide excellent material as you continue to learn to
program. You can take these pseudocode algorithms and turn them into working
programs. If you can't do that yet, don't worry - clearly expressing ideas
really is the hard part, so focus on expressing your ideas rather than
implementing them fully. You can always redo an implementation, but if key
abstractions are obscured in code then it'll be difficult to work with the code
at all.

## References

* [Low-ceremony method](http://c2.com/cgi/wiki?LowCeremonyMethod)
* [Levels of abstraction](http://c2.com/cgi/wiki?LevelsOfAbstraction)
