# Lesson 015 - Refactoring - Small Steps

### Running the exercises

See the top-level README for how to run the exercises. This lesson's exercises
are in the `lessons/015_refactoring_small_steps` directory.

If you run into problems, it may be because your vagrant box is out of date. You
can run `vagrant box outdated` to check, and then `vagrant box update` to update
the image. You then need to destroy your local box and restart it, with `vagrant
destroy` and `vagrant up`.

--------

# Small Steps

Refactoring is a process of uncovering meaning. If you do small refactorings,
you reveal meaning bit-by-bit. If you make large refactorings, you increase the
likelihood of breaking things. You'll spend more time trying to wire everything
back together than you would have taken working in small, methodical steps.

I don't like to be confused when I program. I like to understand what's going
on. I like to accurately predict the outcomes of my changes. I like to look at
the code and know exactly what it's doing. I don't like debugging. I have found
that by working in small steps, I spend more time knowing what I'm doing. I
spend less time confused, because when things go wrong I can narrow down the
possibilities.

In the following exercises, you will practice refactoring in small steps.

--------

## Exercise 1 - Writing obscure code

For this exercise, write the worst piece of Ruby code you can. Limit it to 50
lines (if you struggle to write 50 that's fine, just do what you can). Break
every rule you can think of. Make things as obscure as you possibly can. Make
the program do something - just make it impossible to understand what it is
based on the code.

Now that you've got this code...you can refactor it :)

--------

## Exercise 2 - Refactoring in the tiniest steps

By refactoring, you make changes to bring the code more in line with your
understanding of the system. If you work in small steps, you have a good chance
of each one succeeding and moving you closer to your goal of clean code. When
something doesn't work, you can reverse your decision easily and with minimal
cost.

If you work in large steps, you're more likely to run into problems. You can
troubleshoot them, but now you're not focused on the actual problem. Most
importantly, you're in a state of confusion. You refactor in order to gain
understanding, so if your actions leave you more confused than you were before,
they're probably the wrong actions. Working in small steps might feel slow or
unnecessary, but they allow you to work from a place of understanding and
confidence.

I've mentioned small steps and big steps, how can we quantify them? I don't have
any hard rules. I ask a few questions about some code to help me get an idea of
how big or small a possible change might be.

Follow this process for your obscure code from exercise 1. For every line of
code ask:

1. What does it mean? (NOT what does it do)
2. Does it depend on any lines of code before it?
3. Do any lines of code after it depend on it?

You don't have to track the actual dependencies between lines, just make a note
of whether that line depends on any before it or if any after it depend on it.

Now start with the low-hanging fruit. You want to look at lines of code with
unclear meanings, and no dependencies. Extract those to variables or methods as
discussed in lesson 005. Follow JB's two rules of simple design: minimize
duplication and fix bad names.

Once you've finished that, go to work on the lines that depend on previous
lines. Group them together, and begin by extracting the smallest groups to new
methods. When you see duplication in the method names or in argument lists,
consider extracting a new class.

This process is iterative. After extracting some code, you have a slightly
different structure. You might have made something more clear, but introduced a
bit of duplication. After each step, think back to the simple rules and try to
make the new smallest possible change.

### How much is enough?

You could refactor forever, but that's not practical. How far should you go? How
much is good enough, and when should you stop?

I follow a few simple guidelines. I stop refactoring when:

1. I'm satisfied
2. I've left the code better than I found it

2 is really the bare minimum, and 1 is the stopping point. It's subjective, but
I know when I feel good about a piece of code. When I feel good, I move
on. Sometimes it would take more time than I have to get it to a point where I
feel good about it, in which case I follow Uncle Bob Martin's Boy Scout Rule:
leave it better than you found it. If you do that, even just a little bit, the
system will improve over time without you even noticing.

--------

## Exercise 3 - Refactoring your own code

You should be capable of refactoring in small steps at this point. You've
learned powerful extraction techniques in lesson 5, and in the previous exercise
I gave you a method for evaluating possible changes and choosing the simplest
ones. Now choose a piece of code that you're reasonably familiar with and use
these techniques to refactor it. Start with small changes that you can make
independent from anything else. Then work your way up, identifying lines of code
that depend on one another and extracting them to methods where it makes
sense. Work methodically. When you're refactoring, you know the result of your
change. The system should always work the same as it did before. If you make a
change and a test fails, ask yourself why. Did you make a typo when you made the
change? Or did you take too big of a step, and miss something important about
how the code executes?

--------

# Wrap up

I asked you to write obscure code so that you'd be forced to express your coding
values in a different way. If you intentionally create a piece of ugly code,
that can give you some insight into what you think clean code means. When you
refactor it, you see each side of the refactoring as a different expression of
the same idea. You practice the habit of transforming code according to your
values.

When you refactor, try working in small steps. If it feels slow, set a
timer. I think you'll be amazed at how fast you actually go. When you can string
together a series of small changes without getting stuck, you can accomplish a
lot. This goes whether you're refactoring or adding new functionality. I find
the approach particularly useful when refactoring because I'm starting from a
point of low understanding to begin with. I always prefer to make a small change
that increases my understanding rather than a bigger change that leaves me confused.

--------

## References

* [The Boy Scout Rule](http://programmer.97things.oreilly.com/wiki/index.php/The_Boy_Scout_Rule)
