class ResourceAccessControl
  def initialize(resource)
    @resource = resource
  end

  def can_show?(user)
    if @resource.private?
      user.username == 'admin'
    else
      user.username != 'anonymous'
    end
  end

  def can_edit?(user)
    user.username == 'admin'
  end
end

# a controller action might look like:
#
# def edit
#   unless ResourceAccessControl.new(current_resource).can_edit?(current_user)
#     head :unauthorized
#   end
# end
