module FluffyIdiom
  def self.included(klass)
    klass.extend ClassMethods
    define_fluffy_methods klass
  end

  def self.define_fluffy_methods(klass)
    klass.singleton_class.class_eval do
      define_method("fluffy_#{klass.to_s.underscore}") do
        new_fluffy
      end
    end

    klass.class_eval do
      attr_reader :fluffy unless instance_methods.include?(:fluffy)
      attr_writer :fluffy unless instance_methods.include?(:fluffy=)
    end
  end

  module ClassMethods
    def new_fluffy
      new.tap {|m| m.make_fluffy }
    end
  end

  def fluffy?
    !!fluffy
  end

  def make_fluffy
    self.fluffy = true
  end
end
