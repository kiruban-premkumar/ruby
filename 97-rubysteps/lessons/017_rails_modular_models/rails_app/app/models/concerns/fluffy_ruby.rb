module FluffyRuby
  module ClassMethods
    def new_fluffy
      new.tap {|m| m.make_fluffy }
    end

    def define_fluffy_methods
      klass_name = self.to_s.underscore

      singleton_class.class_eval do
        define_method("fluffy_#{klass_name}") do
          new_fluffy
        end
      end

      attr_reader :fluffy unless instance_methods.include?(:fluffy)
      attr_writer :fluffy unless instance_methods.include?(:fluffy=)
    end
  end

  def fluffy?
    !!fluffy
  end

  def make_fluffy
    self.fluffy = true
  end
end
