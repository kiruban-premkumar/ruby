module AccessControlConcern
  def can_show?(user)
    user.username != 'anonymous'
  end

  def can_edit?(user)
    user.username == 'admin'
  end
end

# a controller action might look like:
#
# def edit
#   unless current_resource.can_edit?(current_user)
#     head :unauthorized
#   end
# end
