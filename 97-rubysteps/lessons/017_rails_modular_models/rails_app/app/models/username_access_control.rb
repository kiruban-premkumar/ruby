class UsernameAccessControl
  def can_show?(user)
    user.username != 'anonymous'
  end

  def can_edit?(user)
    user.username == 'admin'
  end
end

# a controller action might look like:
#
# def edit
#   unless UsernameAccessControl.new.can_edit?(current_user)
#     head :unauthorized
#   end
# end
