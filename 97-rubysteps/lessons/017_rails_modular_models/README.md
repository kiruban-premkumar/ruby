# Lesson 017 - Rails - Modular Models

### Running the exercises

See the top-level README for how to run the exercises. This lesson's exercises
are in the `lessons/017_rails_modular_models` directory.

If you run into problems, it may be because your vagrant box is out of date. You
can run `vagrant box outdated` to check, and then `vagrant box update` to update
the image. You then need to destroy your local box and restart it, with `vagrant
destroy` and `vagrant up`.

--------

# Modular Models

As you develop Rails applications, you will want to organize your code around
responsibilities. This lets you easily understand your code within a single
project, and reuse code on that project or other projects.

I don't treat Rails applications very differently from any other Ruby
application. Rails provides a ton of functionality and a lot of good ideas I can
use. However, it doesn't make decisions for me. I make decisions, and it just so
happens that Rails supports those decisions for the most part.

You can evolve your design over time. You can start off by just throwing
everything into a model and then refactoring as you go. I've done that plenty of
times. You can also pay more attention to the different responsibilities in your
system and organize your code to make those responsibilities cohesive and
decoupled from one another.

By cohesive, I mean the property of all parts fitting together and being related
to one another. By decoupled, I mean the property of two things being fully
isolated from one another. When you decouple two things, you can reason about
them independently.

I think of cohesion and coupling in terms of degrees, rather than absolutes. An
object will be more or less cohesive, and two objects will be somewhere between
loosely coupled and tightly coupled, or fully decoupled.

Rails provides a default structure for our code. Within that structure, we need
to organize all of the custom behavior. We have looked at the basics of defining
and organizing behavior in previous lessons. In this lesson, we will look at how
to use those techniques in a Rails context - with the model, specifically.

--------

## Exercise 1 - As it concerns Ruby

Rails has supported concerns since version 3. Concerns wrap up some common Ruby
idioms and provide automatic dependency resolution between modules.

I don't use concerns, and I'll show you why in this exercise. I never cared for
the idioms that concerns wrap up in the first place. I think you can get the job
done with normal Ruby calls, with very little code.

"Concerns" has a special meaning in the programming world, typically referring
to "cross-cutting concerns" which are behaviors that can exist across many
different kinds of objects and are not unique to any particular kind of
object. Logging, caching, tagging... anything that ends in "ing" maybe? Well,
maybe not anything, and definitely not everything.

Rails doesn't particularly stick to the original definition of concerns, but we
can use it for inspiration all the same. Designing our code around concerns will
help us write modular code that we can easily maintain and test.

Your tools of choice for designing around concerns will be classes, modules, and
basic metaprogramming. Each tool lets you express your ideas a bit differently.

In `rails_app/app/models/concerns/fluffy_idiom.rb` I have defined a module that
wraps up a common Ruby idiom. By using the `Module#included` callback, simply
including a module allows you to run any arbitrary code on the class that
included the model. Libraries often automatically extend a `ClassMethods` module
as part of the callback. They might run other methods as well. Look inside
`rails_app/app/models/simple_model.rb` to see a class that includes this
module. Run the rails console with `rails c` and experiment with the model. Can
you identify all the new methods available to the model?

I don't like the idiom because I don't like automatically running code as part
of including a module. I understand including a module to define new instance
methods on a class. I understand extending a module to define new class
methods. Each one of those actions defines new methods that I can call. I prefer
including and extending modules to stick to making their methods available to
the class, and making any other behavior explicit through a method call.

In `rails_app/app/models/concerns/fluffy_ruby.rb` I've written an alternative
implementation for our fluffiness concern. Uncomment the relevant lines in
`rails_app/app/models/simple_model.rb` and comment out the `include FluffyIdiom`
call. Now reload the Rails app in the console by running `reload!` and then play
around with another instance of `SimpleModel`. It's three lines of code to
include all of the behavior versus one, but the lines explicitly state the three
parts of including all the new behavior. Nothing happens magically - I know via
the method name `define_fluffy_methods` that it will do some metaprogramming.

--------

## Exercise 2 - A non-persistent model

Persistence plays a huge part in Rails applications, but you don't need to jam
everything in to persistent classes. You can delegate to Plain Ruby Objects
("PROs") that represent a non-persistent but meaningful part of the model.

You can move most behavior to PROs if you want. They can either work in a higher
layer and coordinate persistent objects, or the persistent objects can delegate
to PROs. You can use either approach where it makes sense, just be sure to
organize your code and use good naming conventions.

Take a look at `rails_app/app/models/username_access_control.rb` where you'll
see a simple example of a permission system. The persistent model doesn't have
to know anything about it, and you can write a mini framework to use it
automatically as shown in lesson 012. If you already have something like that in
place, it's a simple change to introduce access control.

Once you have a place for some behavior, you can refactor. You can introduce
more sophisticated behavior. In
`rails_app/app/models/resource_access_control.rb` I've written another potential
implementation for access control. It also knows about the persistent model,
which remains ignorant of the access control layer.

Simple models like this can help to keep the rest of the code clean. Do not be
afraid to create lots of small models if they make sense as abstractions. You
should always look out for duplication in names, meaning, or intent among your
classes. Duplication in general means a missing abstraction, and if you have
duplication among classes then you are likely missing a key abstraction. Look
for it in the model and refactor to make it explicit and eliminate duplication.

I have shown two simple mechanisms for access control, but there are other
ways. Try creating one that uses roles - how does the model change? You could
also pretend to make a call to another authorization server, passing along
whatever relevant info you want. Can you maintain the basic interface of
`#can_show?(user)` and `#can_edit?(user)` yet radically change the
implementation?

Think of any other rules that you might have around how a user can access a
protected resource, and try to encode them as PROs.

--------

## Exercise 3 - Mixing into the model

The style I've shown you so far produces modular code. You can modify, add, and
replace modules with little to no effect on other modules. In this sense,
"modules" means any modular organization of code. Of course, Ruby modules
provide a great way to organize your code in a modular way, hence the name.

You can test the policy-style PROs, regardless of your testing style. They work
with mocks, they work with "real" objects. You can instantiate them easily, and
they have a clearly-defined interface which means you can use them
polymorphically. The implementation couples the access control objects to
duck-typed interfaces consisting of the messages `#username` and `#private?`. We
can easily create objects that respond to either of those messages, making this
a perfectly acceptable level of coupling in my mind.

You might prefer a less explicit style, with objects having lots of behavior
defined by several modules. You might find this style more readable or more
Ruby-like. In `rails_app/app/models/concerns/access_control_concern.rb` I've
adapted the previous access control class to a module that we can include in any
class. I simply changed `class` to `module` and now we have behavior sharable
via `include` and `extend`. This shows that code structure helps support certain
perspectives. If you build up behavior using include, you'll have objects that
have lots of behavior at runtime. You run the risk of creating hidden
dependencies between modules. If you can avoid that, by keeping your modules
cohesive and decoupled from one another, then you can write simple, expressive
code at a high level that drives sophisticated functionality from the included
modules. By using classes, you introduce the need for an explicit collaboration
between two objects. You can choose either approach based on the situation or
personal taste.

--------

# Wrap up

Rails newbies frequently make the mistake of defining most of an application's
behavior in classes that double as a persistence layer. Those with experience
prior to Rails often wonder how they're supposed to get away from that
approach. Both groups can do the same thing to help themselves: write as much of
your code in "plain" Ruby code as you can. Make your core ideas explicit,
cohesive, and decoupled.

You always have a choice when it comes to structuring code. Don't waste time
trying to find the best choice. Try something that you know works, and then get
feedback. Do you like the code? What keeps changing? What keeps breaking?

Whether you're working on a small or big application, organizing your code into
modules and keeping them fully decoupled from one another will help you create a
codebase that is easy to understand and maintain, and that works reliably. Ruby
lets you play with organization by providing a source code mechanism and a
run-time mechanism. You can write good, clean code using any number of
techniques. Each one provides different benefits and has potential downsides. In
future lessons, we will continue to explore the possibilities of organizing and
expressing ideas in Ruby. Take what you've learned in this lesson and start
writing more cohesive, modular code.

--------

## References

* [Coupling and Cohesion](http://c2.com/cgi/wiki?CouplingAndCohesion)
* [Corey Haines doesn't use concerns](http://blog.coreyhaines.com/2012/12/why-i-dont-use-activesupportconcern.html) (or at least didn't as of 12/12)
* [ActiveSupport::Concern](http://api.rubyonrails.org/classes/ActiveSupport/Concern.html)
* [Cross cutting concerns](http://c2.com/cgi/wiki?CrossCuttingConcern)
* [Orthogonality](http://c2.com/cgi/wiki?DefinitionOfOrthogonal)
* [Strategy pattern (aka policy pattern)](http://en.wikipedia.org/wiki/Strategy_pattern)
