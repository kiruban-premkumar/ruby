describe 'RubySteps', '#handle_paypal_email()' do
  let(:ruby_steps) { RubySteps.new(mailing_list) }

  let(:mailing_list) { MailingList.new }
  let(:member) { Member.new(email_address) }
  let(:email) { PaypalEmail.new(email_address) }
  let(:email_address) { 'joe@example.com' }

  before do
    mailing_list.add_free_member(member)
  end

  it 'upgrades the member to a paid subscription and invites them to the forum' do
    ruby_steps.handle_paypal_email email
    expect(ruby_steps).to be_paid_member(email_address)
    expect(ruby_steps).to be_invited_to_forum(email_address)
  end
end
