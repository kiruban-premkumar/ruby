require 'set'

class RubySteps
  def initialize(mailing_list)
    @mailing_list = mailing_list
  end

  def handle_paypal_email(email)
    @mailing_list.upgrade_user email.member_email_address
  end
end

class MailingList
  attr_reader :paid_users, :free_users

  def initialize
    @paid_users = Set.new
    @free_users = Set.new
  end

  def add_free_member(member)
    @free_users << member
  end

  def upgrade_user(email_address)
    @free_users.detect {|u| u.email_address == email_address }.tap do |u|
      @paid_users.add u
      @free_users.delete u

      u.upgrade
    end
  end
end

class Member
  attr_reader :email_address

  def initialize(email_address)
    @email_address = email_address
  end

  def upgrade
    @upgraded_at = Time.now
  end

  def paid?
    !!@upgraded_at
  end
end

class PaypalEmail
  attr_reader :member_email_address

  def initialize(member_email_address)
    @member_email_address = member_email_address
  end
end

describe 'RubySteps', '#handle_paypal_email()' do
  let(:ruby_steps) { RubySteps.new(mailing_list) }

  let(:mailing_list) { MailingList.new }
  let(:member) { Member.new(email_address) }
  let(:email) { PaypalEmail.new(email_address) }
  let(:email_address) { 'joe@example.com' }

  before do
    mailing_list.add_free_member(member)
  end

  it 'upgrades the member to a paid subscription' do
    ruby_steps.handle_paypal_email email
    expect(member).to be_paid
  end
end
