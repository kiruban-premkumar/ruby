# Lesson 013 - TDD - Layer Cake

### Running the exercises

See the top-level README for how to run the exercises. This lesson's exercises
are in the `lessons/013_tdd_layer_cake` directory.

If you run into problems, it may be because your vagrant box is out of date. You
can run `vagrant box outdated` to check, and then `vagrant box update` to update
the image. You then need to destroy your local box and restart it, with `vagrant
destroy` and `vagrant up`.

--------

# TDD Layer Cake

You can write all different kinds of tests: unit tests, functional tests,
acceptance tests, load tests, smoke tests...and plenty more that I don't know
about I'm sure.

I've said before that each type of test gives you a different kind of benefit. I
think I was wrong when I've said that. Each type of test, like any investment in
software, should give you some business value. In essence, tests should help you
a) make money b) save money or c) learn.

I have yet to write a test that makes money, but I have written plenty that save
money and have helped me and others learn.

In this lesson, we'll look at different ways we can learn as a result of writing
tests. In particular, we'll look at possible outcomes from writing unit and
acceptance tests and why you might choose to write either kind of tests. First
though, I want to take a look at what it means to write a test.

I treat writing tests with the same economic mindset as I do writing production
code. In Lesson 007 I talked about the economics of software projects and how we
write computer programs to automate business processes. Each minute you spend
writing code is a minute that's expected to pay off in some way, typically by
saving you or someone else more time than it took you to code. You also need to
factor in maintenance cost, because once something is in place, you'll need to
fix the occasional problem at the very least. More often, you'll find that once
a software system starts providing value, it has a way of acquiring features in
order to provide new value - as long as there's someone there to work on it.

I think of each automated test as its own computer program. It just makes sense
to me that way. Ideally, I can run each test independently of every other
test. In my mind, a test suite is just a collection of computer programs I've
written to automate some work that I'd need to do manually, a way to save myself
lots of time.

If you don't understand the value of tests in saving time, all you need to do is
refactor some nasty code with and without tests. Without tests, you have to
constantly run the program and, yes, test it to see if everything still
works. All those commands you type into irb could go into tests that the
computer runs whenever you want.

TDD gives you the benefit of that test suite up-front. You don't need ugly code
to benefit from an effective test suite. In fact, an effective test suite really
becomes valuable when you have clean code that you can change easily. You can
make changes quickly and easily, and get quick verification that you didn't
break anything. This lets you rapidly respond to new requests and build a system
that grows in value over time. It's a fun, smart way to work.

In this lesson though, I'm going to look at that other benefit that I've talked
about so often - learning.

## Learning with TDD

I learn best by taking action, and then reflecting on the outcomes of my
actions. I've seen this modeled in other people, and I've seen it work in
teams. Agile revolves around this approach. We show up every day and go to work
according to a shared game plan. We regularly stop to reflect on what we've
done, and figure out what to do next.

TDD has the ebb-and-flow of learning built right into it. You have an idea for a
test. You put the idea into action, and write the test. You run it and get
feedback. You now plan out what to do next - fix the test, fix the code, delete
the test, whatever. You take that action, run the tests, get your feedback. At
each step along the way, you'll have moments of action and reflection. I can
best describe these as cycles that operate inside of other cycles.

For example, you have the cycle of delivering new features. You take the idea
for a feature, you work on it, and then move on to the next one. That "you work
on it" represents a lot of other cycles going on.

One such possible cycle, of course, is TDD. Each test run provides feedback, and
an opportunity to reflect and decide what to do next. Each new passing test
means a chance to stop and consider possible next steps. You learn as a result
of doing TDD because TDD is essentially the learning process applied to code.

--------

## Exercise 1 - Learning from the outside-in

I don't make a big point of working from the outside-in versus the inside-out,
or bottom-up, or however people choose to explain directionality of software
development. At some point, you're going to find yourself building new things
out of things that already exist, and you're going to find yourself building
around things that don't yet exist. It helps to know how to work either way of
working.

I will arbitrarily start this exercise working outside-in. I tend to understand
things more when I work out a big picture first before the details. I find this
way of working opens up a lot of questions to ask and possibilities to explore.

If you don't mind me being a bit self-indulgent, I'd like to take another look
at how I might model RubySteps in code. I've already written some software to
automate repetitive tasks, and I suspect I'll need to write more. I think you'll
benefit from seeing exactly how I think about writing computer programs in terms
of providing business value.

### The setup

I'll start by just writing a couple notes about RubySteps. Just like in lesson
007, I'll refactor the English and then start turning it into code ideas.

* A site visitor fills out the rubysteps form
* the system sends a confirmation email
* the visitor clicks the confirmation email
* the new rubysteps member receives a welcome email

That's the signup process for free users - greatly simplified since I made the
repository public on GitHub. Now let's look at when someone chooses to pay:

* RubySteps free member pays via PayPal
* PayPal sends me an email
* I update the subscription status
* I add the member to GitHub
* I invite the member to discourse

The free signup is completely automated, and the paid signup is not. I could
spend time learning about and modeling the free signup, but I don't need to
automate it. I think I can learn what I need to by focusing on automating the
paid setup.

Let's refactor that to some code:

```ruby
member_paid_via_paypal
received_email_from_paypal
update_subscription_status
add_member_to_github
invite_member_to_discourse
```

I see member a couple times - no surprise there. I see some other stuff about
PayPal, and then a couple things about integrating with other systems. This
actually seems like a lot to tackle all at once, so I'll focus on the first part
of handling a notification from PayPal and updating the subscription status.

I'll start by writing an acceptance test. I use this acceptance test to agree on
what it means to deliver some value for this feature or bug fix. It also defines
boundaries for me to work within and interfaces for me to support. You can write
an acceptance test a number of ways, and for now I will use a relatively simple
RSpec test. Take a look at `exercise_1a_acceptance_spec.rb` for an example. It
won't run, I'm just starting to work out some ideas.

Right off the bat, I've done something important. I've started to define a basic
contract for this piece of software. It is a Ruby library that I can require and
interact with myself. There might be a command-line app or a web app down the
road, but as of now the official interface is through a Ruby API.

Now I can start to ask questions. What if the email from PayPal is not for a
payment? What if it's a refund request, or a cancellation? What if it's just a
notice from PayPal? How do I determine whether it's for a payment or something
else? Does the system need to handle any other kinds of emails?

By asking these questions and exploring the answers to them, you learn more
about your system, your process, and your business. You could use any number of
ways to drive out those questions - I've chosen to sketch out 7 lines of Ruby.

At this point, I have a decision to make. I'm not going to scrap the test,
because I think it's a good starting point. I see at least three objects that I
could work on: `ruby_steps`, `member`, and `email`. Because `ruby_steps` is the
most interesting and unclear, and has the most specific behavior in terms of
`handle_paypal_email`, I'll start there.

I've made `exercise_1b_rubysteps_spec.rb` as a place to start writing unit tests
for a `ruby_steps` object. I am testing the `handle_paypal_email` method
specifically, and have written out a few pending examples of what it should
do. Because I still haven't figured out the interface and the object's
collaborators, I'm leaving the acceptance test untouched for now. I can update
it once I drive out a working structure.

We need to turn that pending spec into an expectation. Go ahead and write one
yourself. You can see what I did in `exercise_1c_rubysteps_spec.rb`. I've chosen
to use a mock-based expectation. I personally find mocks quite useful, and tend
to do state-based expectations in my acceptance tests. In any case, you could go
about it any way but this is what I've chosen for now.

It still doesn't run, and now I need to figure out where this `mailing_list`
object and the email address come from. I've also got this unspecified `email`
object, which I think can be the source for the email adress. Take a look at
`exercise_1d_rubysteps_spec.rb` to see how I've set that up. Now that I have
something that looks like a complete unit test, I can implement enough to get to
the actual test failure. See `exercise_1e_rubysteps_spec.rb` for my changes. Run
this spec from the command line. I can make that test pass with a simple method
call. See `exercise_1f_rubysteps_spec.rb`. Great, I've now learned that I have
something called a `mailing_list` and it knows about email addresses and how to
upgrade users. I can write unit tests in order to drive its implementation.

In `exercise_1g_mailing_list.rb` I've got a simple spec to validate the behavior
of the mailing list. It exposes yet another interface that I need to explore,
and forces me to ask some questions. What if the user is not on the list at all?
What if the user is already upgraded? You will need to decide how to address
those. What tests can you write to handle those cases, and what do you learn
from writing them? Plus keep in mind that this likely isn't the final
implementation - I'm going to want to make API calls to the actual mailing list
software. At least I have an idea of the interface I want to use.

--------

## Exercise 2 - Tweaking the acceptance test

I don't have a functioning system at this point, but I do have a rough idea of
where I'm going with the acceptance test, and I've clarified some of the details
with unit tests. I think I have enough that I can revise the acceptance test a
bit. Take a look at `exercise_2a_acceptance_spec.rb` to see how I've added some
of the pieces I've built via unit tests. This leaves only the `member` and
`email` objects undefined. I'm not going to bother with a test for `email` right
now since I can treat it as a data object. `member` is more interesting because
it has this method `#paid?` on it as we see in the acceptance test.

I want to get this acceptance test passing before I change it, so I've
implemented the few remaining pieces. The key thing to note is that I changed
the implementation of `MailingList#upgrade_user` to notify the `member` object
of its upgrade status. This allows the `member` to return `true` to `#paid?` and
for the acceptance test to pass. See `exercise_2b_acceptance_spec.rb`.

I think the acceptance test knows a little too much about the details of the
system. It might be okay in the case of a library, but for this example I want
to close the interface a bit more. Now that I have a working acceptance test,
and some unit tests to cover the behavior, I can change the interface. See
`exercise_2c_acceptance_spec.rb` for an example. I've added a new method to
`RubySteps` to check an email: `RubySteps#paid_member?(email_address)`. That
lets me encapsulate the details of the RubySteps system a bit more, and make
explicit the behavior that I want from it. I was able to collapse the list of
users on the mailing list from one collection to two - it turns out that the
fine-grained unit test I wrote to verify moving the user from the free list to
the paid drove out a design that I didn't really need. That's why I tend to
write my tests at a high level and focus on behavior rather than state - it's
easy for me to write state-based tests that drive interfaces I don't care about.

--------

## Exercise 3 - Adding features, guided by tests

Now that there's a basic system in place, I want you to add a new feature. It
makes sense to add this to the high-level acceptance test, and then drive the
actual functionality via unit tests. Look at `exercise_3a_acceptance_spec.rb`
for an example of the changed acceptance test. The new feature is inviting
someone to the discussion forum. I want you to take a stab at the acceptance
test and implementation. Be sure to write unit tests for any new functionality.

What new objects and messages do you discover? How are they similar to or
different from existing objects and messages in the system? Can you collapse
some of the messages and introduce polymorphism? Where might that make sense?

--------

# Wrap up

In this lesson, I've shown you examples of using TDD at an acceptance and unit
test level. I've tried to demonstrate the kinds of things you'll learn as you
write and implement the tests. Acceptance tests tend to inform how clients use
the software, and clarify interfaces. Unit tests tend to drive out the structure
of different objects and the messages they send.

When you do TDD, you tap into your own ability to learn. You provide yourself
with a structured approach to experimenting with ideas, reflecting, and
adapting.

Writing tests helps you clarify your ideas. Sometimes you just need to get ideas
out of your head and into code. Sometimes you have new ideas as a result of
translating your ideas into code. Sometimes as a result of refactoring, you see
ideas in code that you didn't know were there before.

I work with the acceptance and unit TDD cycles as part of my work. I typically
have an acceptance test which drives some new functionality, and then use unit
tests to drive the implementation. Even when it's a small bug, I look to see if
there's an acceptance test that I could change to expose it. I like to test
behavior from the user's perspective as much as possible. With acceptance tests,
we can capture the overall experience. With unit tests, we can capture the
specific behavior of a piece of code.

In future lessons, I will give you many more examples of using different levels
of tests. With practice, you'll be able to write tests at the level you need in
order to learn what you need to know next.

--------

## References

* [Outside-in vs inside-out - Comparing TDD approaches](http://blog.mattwynne.net/2010/08/31/outside-in-vs-inside-out-comparing-tdd-approaches/) - Matt Wynne
* [Layer Cake](http://www.imdb.com/title/tt0375912/) - IMDB
