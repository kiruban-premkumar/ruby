describe 'RubySteps', '#handle_paypal_email()' do
  let(:ruby_steps) { RubySteps.new(mailing_list) }

  let(:mailing_list) { MailingList.new }

  before do
    mailing_list.add_free_member(member)
  end

  it 'upgrades the member to a paid subscription' do
    ruby_steps.handle_paypal_email email
    expect(member).to be_paid
  end
end
