describe 'RubySteps', '#handle_paypal_email()' do
  it 'upgrades the member to a paid subscription' do
    ruby_steps.handle_paypal_email email
    expect(member).to be_paid
  end
end
