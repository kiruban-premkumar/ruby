require 'set'

class MailingList
  attr_reader :paid_users, :free_users

  def initialize
    @paid_users = Set.new
    @free_users = Set.new
  end

  def add_free_member(member)
    @free_users << member
  end

  def upgrade_user(email_address)
    @free_users.detect {|u| u.email_address == email_address }.tap do |u|
      @paid_users.add u
      @free_users.delete u
    end
  end
end

describe MailingList, '#upgrade_user(email_address)' do
  let(:mailing_list) { MailingList.new }

  let(:member) { double('member', email_address: email_address) }
  let(:email_address) { 'joe@example.com' }

  before do
    # we have to get them on somehow, we'll assume the objects come
    # from somewhere else for now
    mailing_list.add_free_member(member)
  end

  it 'adds the user to the paid list' do
    expect(mailing_list.paid_users).to_not include(member)

    mailing_list.upgrade_user email_address

    expect(mailing_list.paid_users).to include(member)
  end

  it 'removes the user from the free list' do
    expect(mailing_list.free_users).to include(member)

    mailing_list.upgrade_user email_address

    expect(mailing_list.free_users).to_not include(member)
  end
end
