describe 'RubySteps', '#handle_paypal_email()' do
  it 'upgrades the user with that email address' do
    expect(mailing_list).to receive(:upgrade_user).with('joe@example.com')
    ruby_steps.handle_paypal_email email
  end
end
