class RubySteps
  def initialize(mailing_list)

  end

  def handle_paypal_email(email)

  end
end

describe 'RubySteps', '#handle_paypal_email()' do
  let(:ruby_steps) { RubySteps.new mailing_list }

  let(:email) { double('email', member_email_address: 'joe@example.com') }
  let(:mailing_list) { double('mailing list') }

  it 'upgrades the user with that email address' do
    expect(mailing_list).to receive(:upgrade_user).with('joe@example.com')
    ruby_steps.handle_paypal_email email
  end
end
