#!/bin/bash

apt-get update -y
apt-get upgrade -y
apt-get autoremove -y
apt-get install -y build-essential libssl-dev libsqlite3-dev libpq-dev libreadline6 libreadline6-dev git nodejs libffi-dev

if [ ! -f /usr/local/bin/ruby ]
then
  wget http://cache.ruby-lang.org/pub/ruby/2.1/ruby-2.1.2.tar.gz
  tar zxf ruby-2.1.2.tar.gz
  cd ruby-2.1.2
  ./configure && make && make install

  gem install bundler
fi
